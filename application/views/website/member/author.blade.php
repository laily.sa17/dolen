@extends('website.template')

@section('title')
 Author
@endsection

@section('content')
<div class="container">
	<div class="col-md-2"><br><br><br>
		<img class="img-responsive" src="{{base_url()}}{{img_holder('profile')}}" align="middle  "style="width:126.66px;height:126.66px;" >
	</div>z
	<div class="col-md-10">
        <br><br><br>
		<h2>{{$profile->name}} <span class="text-muted small">({{$profile->username}})</span></h2>
		<h3><span class="text-muted small">{{ count($profile->blog) }} ARTICLES</span></h3>
  		@if($loggedin == true)
		<a href="{{base_url('member/profile/edit')}}">Edit Profile</a>
		@endif
	</div>
    <div class="col-md-12">
    <div class="panel_title"><br>
        <div>

            <h4><a href="blog.html">Artikel </a></h4>
        </div>
    </div><!-- End Panel title -->
    </div>
    <div class="row">
        <div class="rows">
            @foreach($profile->blog as $blog)
                <div class="col col_4_of_12">
                    <!-- Layout post 1 -->
                    <div class="card">
                        <div class="layout_post_1">
                            <div class="item_thumb">
                                <div class="thumb_hover">
                                    <a href="{{$blog->url}}"><img src="{{$blog->imagedir}}"  style="object-fit: cover;height: 250px;"></a>
                                </div>
                                <div class="thumb_meta">
                                    @foreach($category->where('id', $blog->id_category) as $cate)
                                        <span class="category" jQuery><a href="{{$blog->category->url}}">{{ $cate->name }}</a></span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="item_content">
                                <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                <p>{!! read_more($blog->description,200) !!}</p>
                                <div class="item_meta clearfix">
                                    <span><a href="{{ base_url('author') }}/{{ $blog->penulis->username }}"><i class="fa fa-user"></i> {{ $blog->penulis->name }}</a></span>
                                    <span class="meta_date">{!! waktu_lalu($blog->created_at) !!}</span>
                                    <span class="fa fa-eye">{!! k_view($blog->view) !!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection