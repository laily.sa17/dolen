@extends('website.member.template')

@section('title')
    Profile
@endsection

@section('content')
    <div class="container">
        <div class="panel_title">
            <div>
                <h4>Profile</h4>
            </div>
        </div>
        <div class="panel_title">
        </div><!-- End Panel title -->
        <div class="form-group">
            <br>
            <div class="row">
                <form id="form-daftar">
                    <div id="wrap-image">
                    <div class="col col-md-3">
                        <div style="margin-bottom: 10px">
                            @if($profile->imagedir == null)
                                <img class="product" style="margin: auto; padding: 10%"
                                     src="{{base_url()}}{{img_holder('profile')}}">
                            @else
                                <img class="prodzuct" style="margin: auto; padding: 10%" src="{{$profile->imagedir}}">
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="file" name="image" accept="image/*" class="file-styled image-product-upload">
                        </div>
                    </div>
                    </div>
                    <div class="col col-md-9">
                        <div class="item_content" style="margin-top: 5%; margin-left: 5%">
                            <h2>{{$profile->username}}</h2>
                            <h5 style="margin-top: -2%">{{$profile->email}}</h5>
                        </div>
                        <hr>

                        <div class="form-group" style="margin-top: 5%; margin-left: 5%;">
                            <label class="col-lg-3 control-label">Nama Lengkap<span class="text-danger"><b>*</b></span></label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" placeholder="Nama Lengkap" name="name"
                                       value="{{$profile->name}}" required>
                            </div>
                        </div>
                        {{--<div class="form-group" style="margin-top: 5%; margin-left: 5%;">--}}
                            {{--<label class="col-lg-3 control-label">Nama Lengkap<span class="text-danger"><b>*</b></span></label>--}}
                            {{--<div class="col-lg-9">--}}
                                {{--<input class="form-control" type="file" placeholder="Nama Lengkap" name="bla"--}}
                                        {{--required>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="text-right">
                            <button style="margin-top: 20%" type="submit" class="btn btn-primary">Simpan<i
                                        class="icon-arrow-right14 position-right"></i></button>
                            <a style="margin-top: 20%" class="btn btn-danger" href="javascript:void(0)"
                               onclick="window.history.back(); ">Batalkan <i class="fa fa-times position-right"></i></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="btnReset">
            <button  style="margin-top: -15%; margin-left: 4.5%" class="btn btn-danger" data-toggle="modal"
                     data-target="#exampleModal">
                Reset Password <i class="fa fa-refresh position-right"></i>
            </button>
        </div>
    </div>

    {{--========================================================== Reset Password ==========================================================--}}

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: white">
                    <h4 class="modal-title" id="exampleModalLabel">Reset Password </h4>
                    <button style="margin-top: -2%" type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-ubah" action="{{base_url('member/updatePassword')}}">
                        <div class="form-group">
                            <br><br>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Password Lama<span
                                            class="text-danger"><b>*</b></span></label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="password" placeholder="Password" name="pass_lama"
                                           value="" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Password Baru<span
                                            class="text-danger"><b>*</b></span></label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="password" placeholder="Password" name="password"
                                           value="" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Konfirmasi Password<span
                                            class="text-danger"><b>*</b></span></label>
                                <div class="col-lg-8">
                                    <input class="form-control" type="password" placeholder="Password" name="repassword"
                                           value="" required>
                                    <span class="text-danger" style="display: none;"
                                          id="nsame">Password not match</span>
                                    <span class="text-success" style="display: none;" id="same">Password match</span>
                                </div>
                            </div>
                            <br><br><br>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Simpan<i
                                            class="icon-arrow-right14 position-right"></i></button>
                                <a class="btn btn-danger" href="javascript:void(0)" onclick="window.history.back(); ">Batalkan
                                    <i class="fa fa-times position-right"></i></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/core.css"/>
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/components.css"/>
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/colors.css"/>
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/icons/icomoon/styles.css"/>
    <style type="text/css">
        @media screen and (max-width: 991px) {
          .btnReset{
              margin-top: -2%;
          }
        }
        @media screen and (max-width: 500px) {
            .btnReset{
                margin-top: -8%;
            }
        }
        @media screen and (max-width: 471px) {
            .btnReset{
                margin-top: -8%;
            }
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_layouts.js"></script>

    <script type="text/javascript">
        var editorsmall = false;
    </script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/editor_ckeditor.js"></script>

    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/uploader_bootstrap.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/styling/switch.min.js"></script>
    <script type="text/javascript">
        $("input[name=name]").focus();

        // $(".switch").bootstrapSwitch();
        //
        // $(".select-search").select2({
        //     placeholder: "Pilih Tag Blog",
        //     allowClear: true
        // });

        $(".image-product-upload").change(function (e) {
            var input = this;
            var element = $(this).parents('.col-md-4').find('img');
            var href = $(this).parents('.col-md-4').find('a[rel="gallery"]');

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    jQuery(element).attr('src', e.target.result);
                    jQuery(href).attr('href', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        })


        $("#form-daftar").submit(function (e) {
            e.preventDefault();
            var block = $("#form-daftar");

            $.ajax({
                url: "{{ base_url('member/profile/confirm') }}",
                method: "POST",
                data: $("#form-daftar").serialize(),
                beforeSend: function () {
                    blockMessage($(block), 'Please Wait ,Memperbarui Profile', '#fff');
                }
            })
                .done(function (data) {
                    $('#form-daftar').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'sukses'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                redirect('{{ base_url('member/profile') }}');
                                return;
                            }
                        });

                })
                .fail(function () {
                    $('#form-daftar').unblock();
                    sweetAlert({
                        title: "Opss!",
                        text: "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                        type: "error",
                    });
                })
        })
    </script>
    <script type="text/javascript">
        $("input[name=repassword]").keypress(function (e) {
            var pass = $("input[name=password]").val().substr(0, $("input[name=password]").val().length - 1);
            var rpass = $("input[name=repassword]").val();
            if (pass != rpass) {
                $("#nsame").show();
                $("#same").hide();
            } else {
                $("#nsame").hide();
                $("#same").show();
            }
        })

        $("#form-ubah").submit(function (e) {
            e.preventDefault();
            var block = $("#form-ubah");

            $.ajax({
                url: "{{ base_url('member/updatePassword/confirm') }}",
                method: 'POST',
                data: $("#form-ubah").serialize(),
                beforeSend: function () {
                    blockMessage($(block), 'Please Wait ,Memperbarui Password', '#fff');
                }
            })
                .done(function (data) {
                    $('#form-ubah').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'sukses'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                redirect('{{ base_url('member/profile') }}');
                                return;
                            }
                        });

                })
                .fail(function () {
                    $('#form-ubah').unblock();
                    sweetAlert({
                        title: "Opss!",
                        text: "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                        type: "error",
                    });
                })
        })
    </script>
@endsection