@extends('website.template')
@section('title')
    Login - {{$config->name}}
@endsection

@section('content')
    <section class="main-container col1-layout">
        <div class="main container">
            <div class="col-main">
                <div class="cart wow bounceInUp animated">
                    <div class="gap-sm"></div>
                    <div class="row">
                        <div class="col-sm-6 col-md-offset-3">
                            <div class="box-login" style="margin-top: 20px;">

                                <!-- ==============================Form Login============================== -->

                                <form id="form-login" style="display: block;">
                                    <div class="header">
                                        <h3 style="color: orange; border-bottom:1px solid #eaeaea;margin: 0 0 15px 0;
                      padding: 25px 10px;">Member Login</h3>
                                    </div>
                                    <div class="gap-sm"></div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="username"
                                               placeholder="Email or username" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password" placeholder="Sandi"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <div class="pull-right">
                                            <div class="checkbox">
                                                <label>
                                                    <a href="javascript:void(0)" onclick="showForm_forget('forgot')"
                                                       style="font-size: 13px; margin-left: 160px;">Lupa Password?</a>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" class="btn btn-danger btn-block">Login</button>
                                        </center>
                                    </div>
                                    <div class="form-group">
                                        <!-- <div class="gap-xs"></div> -->
                                        <span class="help-block text-center" style="float: center;">
                      <span style="font-size: 13px;">Belum Punya akun ?</span>
                      <a href="javascript:void(0)" onclick="showForm_daftar('daftar')" style="font-size: 13px;"
                         class="text-primary">Daftar Disini</a>
                    </span>
                                    </div>
                                </form>

                                <!-- ==============================End Form Login============================== -->
                                <!-- ==============================Form Register============================== -->

                                <form id="form-daftar" style="display: none;" action="{{base_url('member/register')}}">
                                    <div class="header">
                                        <h3 style="color: orange; border-bottom:1px solid #eaeaea;margin: 0 0 15px 0;
                      padding: 25px 10px;">Daftar</h3>
                                    </div>
                                    <div class="gap-sm"></div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" placeholder="Nama Lengkap"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="username"
                                               placeholder="Nama Pengguna/Username" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="password"
                                               placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" name="repassword"
                                               placeholder="Ulangi Password" required>
                                        <span class="text-danger" style="display: none;"
                                              id="nsame">Password not match</span>
                                        <span class="text-success" style="display: none;"
                                              id="same">Password match</span>
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" class="btn btn-danger btn-block">Daftar Sekarang
                                            </button>
                                        </center>
                                        <div class="gap-xs"></div>
                                        <span class="help-block text-center" style="float: left;">
                      <span style="font-size: 13px; margin-left:50px;">Sudah terdaftar ?</span>&nbsp;&nbsp;
                      <a href="javascript:void(0)" style="font-size: 13px;" class="text-primary"
                         onclick="showForm_login('login')">Login</a>
                    </span>
                                    </div>
                                </form>

                                <!-- ==============================End Form Register============================== -->
                                <!-- ==============================Form Pass============================== -->

                                <form id="form-forgot" style="display: none;" action="{{base_url('member/forgot')}}">
                                    <div class="header">
                                        <h3 style="color: orange; border-bottom:1px solid #eaeaea;margin: 0 0 15px 0;
                      padding: 25px 10px;">Lupa Password ?</h3>
                                    </div>
                                    Anda Akan Menerima Tautan Untuk Membuka Password Baru<br><br><br>
                                    <div class="gap-sm"></div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="Email Anda"
                                               required>
                                    </div>
                                    <div class="form-group">
                                        <center>
                                            <button type="submit" class="btn btn-danger btn-block">Dapatkan Passowrd
                                                Baru
                                            </button>
                                        </center>
                                        <div class="gap-xs"></div>
                                        <span class="help-block text-center" style="float: left;">
                      <span style="font-size: 13px; margin-left:135px;">Sudah Ingat Password ?</span>&nbsp;&nbsp;
                      <a href="javascript:void(0)" onclick="showForm('login')" style="font-size: 13px;"
                         class="text-primary">Login</a>
                    </span>
                                    </div>
                                </form>

                                <!-- ==============================End Form pass============================== -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('styles')
    <link href="{{base_url()}}admin_assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        .jGrowl.top-center {
            top: 170px;
            color: white;
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/aksa/aksa-js.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/notifications/jgrowl.min.js"></script>

    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/notifications/pnotify.min.js"></script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoadGoogleCallback" async defer></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        $("input[type=text]").focus();
        $("#form-login").submit(function (e) {
            e.preventDefault();
            var block = $("#form-login");

            $.ajax({
                url: "{{ base_url('member/masuk/auth') }}",
                method: 'POST',
                data: $("#form-login").serialize(),
                beforeSend: function () {
                    showBlock($(block), '<i class="fa fa-2x fa-spinner fa-spin"></i>', '#fff');
                }
            })
                .done(function (data) {
                    if (data.auth == false) {
                        ShowNotif('Login Failed!', data.msg, 'top-center', 'bg-danger');
                        $("#form-login").find('input').val('');
                        $(block).unblock();
                        $("input[type=text]").focus();
                        // var go  = setTimeout(function(){
                        //   redirect('{{ base_url('member') }}');
                        // },2000);
                        return;
                    }
                    ShowNotif('Login Success!', 'Redirecting . . .', 'top-center', 'bg-success');
                    var go = setTimeout(function () {
                        redirect('{{ base_url('member') }}');
                    }, 2000);

                })
        })
    </script>

    <script type="text/javascript">
        var base_url = 'https://www.waymarkets.com/';

        function showForm_forget(type) {
            if (type == "forgot") {
                $("#form-login").fadeOut(function () {
                    $("#form-forgot").fadeIn();
                })
            }
            else {
                $("#form-forgot").fadeOut(function () {
                    $("#form-login").fadeIn();
                })
            }
        }

        $("#form-forgot").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-forgot")[0]);

            $.ajax({
                url: $("#form-forgot").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    blockMessage($('html'), 'Please Wait , Processing', '#fff');
                }
            })
                .done(function (data) {
                    $('html').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'Restoring Success'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                $("#form-forgot").find("input").val("");
                                return;
                            }
                        });

                })
                .fail(function () {
                    $('html').unblock();
                    sweetAlert({
                            title: "Opss!",
                            text: "Something Wrong! , Try Again Later",
                            type: "error",
                        },
                        function () {
                        });
                })
        })

        function showForm(type) {
            if (type == "login") {
                $("#form-forgot").fadeOut(function () {
                    $("#form-login").fadeIn();
                })
            }
            else {
                $("#form-login").fadeOut(function () {
                    $("#form-forgot").fadeIn();
                })
            }
        }

        $("#form-login").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-login")[0]);

            $.ajax({
                url: $("#form-login").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    blockMessage($('html'), 'Please Wait , Processing', '#fff');
                }
            })
                .done(function (data) {
                    $('html').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'Restoring Success'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                $("#form-login").find("input").val("");
                                return;
                            }
                        });

                })
                .fail(function () {
                    $('html').unblock();
                    sweetAlert({
                            title: "Opss!",
                            text: "Something Wrong! , Try Again Later",
                            type: "error",
                        },
                        function () {
                        });
                })
        })

        function showForm_login(type) {
            if (type == "login") {
                $("#form-daftar").fadeOut(function () {
                    $("#form-login").fadeIn();
                })
            }
            else {
                $("#form-login").fadeOut(function () {
                    $("#form-daftar").fadeIn();
                })
            }
        }

        $("#form-login").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-login")[0]);

            $.ajax({
                url: $("#form-login").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    blockMessage($('html'), 'Please Wait , Processing', '#fff');
                }
            })
                .done(function (data) {
                    $('html').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'Restoring Success'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                $("#form-login").find("input").val("");
                                return;
                            }
                        });

                })
                .fail(function () {
                    $('html').unblock();
                    sweetAlert({
                            title: "Opss!",
                            text: "Something Wrong! , Try Again Later",
                            type: "error",
                        },
                        function () {
                        });
                })
        })

        function showForm_daftar(type) {
            if (type == "daftar") {
                $("#form-login").fadeOut(function () {
                    $("#form-daftar").fadeIn();
                })
            }
            else {
                $("#form-daftar").fadeOut(function () {
                    $("#form-login").fadeIn();
                })
            }
        }

        $("#form-daftar").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-daftar")[0]);

            $.ajax({
                url: $("#form-daftar").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    blockMessage($('html'), 'Please Wait , Processing', '#fff');
                }
            })
                .done(function (data) {
                    $('html').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'Restoring Success'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                $("#form-daftar").find("input").val("");
                                return;
                            }
                        });

                })
                .fail(function () {
                    $('html').unblock();
                    sweetAlert({
                            title: "Opss!",
                            text: "Something Wrong! , Try Again Later",
                            type: "error",
                        },
                        function () {
                        });
                })
        })
    </script>
    <script type="text/javascript">
        $("input[name=name]").focus();
        $("input[name=username]").keypress(function (e) {
            if (this.value.match(/([ -.*+?^=!;:${}()|\[\]\/\\])/g)) {
                this.value = this.value.replace(/([ -.*+?^=!;:${}()|\[\]\/\\])/g, '');
            }
        })
        $("input[name=username]").blur(function (e) {
            if (this.value.match(/([ -.*+?^=!;:${}()|\[\]\/\\])/g)) {
                this.value = this.value.replace(/([ -.*+?^=!;:${}()|\[\]\/\\])/g, '');
            }
        })


        $("input[name=repassword]").keypress(function (e) {
            var pass = $("input[name=password]").val().substr(0, $("input[name=password]").val().length - 1);
            var rpass = $("input[name=repassword]").val();
            if (pass != rpass) {
                $("#nsame").show();
                $("#same").hide();
            } else {
                $("#nsame").hide();
                $("#same").show();
            }
        })

        $("#form-daftar").submit(function (e) {
            e.preventDefault();
            var block = $("#form-daftar");

            $.ajax({
                url: "{{ base_url('member/daftar/confirm') }}",
                method: 'POST',
                data: $("#form-daftar").serialize(),
                beforeSend: function () {
                    showBlock($(block), '<i class="fa fa-2x fa-spinner fa-spin"></i>', '#fff');
                }
            })
                .done(function (data) {
                    if (data.auth == false) {
                        ShowNotif('Register Failed!', data.msg, 'top-center', 'bg-danger');
                        $("input[type=password]").val('');
                        $(block).unblock();
                        // var go  = setTimeout(function(){
                        //   redirect('{{ base_url('member') }}');
                        // },2000);
                        return;
                    }
                    ShowNotif('Register Success!', 'Check your mail for activation.\nRedirecting...', 'top-center', 'bg-success');
                    var go = setTimeout(function () {
                        redirect('{{ base_url('member') }}');
                    }, 2000);

                })
        })
    </script>
@endsection