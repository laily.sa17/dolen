<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{base_url('assets')}}/css/fontawesome.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/sidebar.css" type="text/css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/custom.css" type="text/css">
    <link rel="stylesheet" href="{{base_url()}}admin_assets/css/minified/components.min.css" type="text/css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <style type="text/css">
        .jGrowl.top-center {
            top: 170px;
            color: white;
        }

        @media (max-width: 768px) {
            .navbar-fixed-top {
                position: fixed !important;
            }
        }

        .alert {
            padding-right: 35px !important;
        }

        .close {
            font-size: 25px !important;
            opacity: 0.4 !important;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('style')
</head>
<body>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ base_url() }}">{{ $config->name }}</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                {{-- <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li><a href="#about">About</a></li>
                  <li><a href="#contact">Contact</a></li>
                </ul> --}}
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">
                            {{ $name }} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{base_url('member/profile')}}"><i class="fa fa-user"></i> Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ base_url('member/logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <div id="sidebar-wrapper" class="bg-side">
        <ul class="sidebar-nav">
            <li class="sidebar-brand"><a href="{{ base_url('member') }}">{{ $config->name }}</a></li>
            <li><a href="{{ base_url('member') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{ base_url('member/post') }}"><i class="fa fa-newspaper-o"></i> Post</a></li>
            <li style="height: 1px;overflow: hidden;background-color: #e5e5e5;"></li>
            <li><a href="{{ base_url('member/logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
        </ul>
    </div>
    <a href="#" style="margin-top: 68px;margin-left: -5px;" class="btn bg-side btn-side" id="menu-toggle"><i
                class="fa fa-bars"></i></a>
    @if($profile->status == 'pending')
        <div class="container">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    Active your email for create new article.
                </div>
            </div>
        </div>
    @endif
    @yield('content')
</div>
<script type="text/javascript" src="{{base_url('assets')}}/js/jquery.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/aksa/aksa-js.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/notifications/jgrowl.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/media/fancybox.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/styling/switchery.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript"
        src="{{base_url()}}admin_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/pickers/daterangepicker.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/notifications/pnotify.min.js"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>
<script type="text/javascript">
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
<script type="text/javascript">
    $('[data-popup=lightbox]').fancybox({
        padding: 3
    });


    function deleteIt(that) {
        swal({
            title: "Apa Anda Yakin ?",
            text: "Anda Akan Menghapus Data Ini",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus Data!",
            closeOnConfirm: false
        }, function () {
            swal({
                title: "Deleted",
                text: "Data Anda Telah Di Hapus",
                type: "success"
            }, function () {
                redirect($(that).attr('data-url'));
            });

        });
    }

    function showTab(that) {
        var element = 'a[href="' + $(that).attr('element-id') + '"]';
        $(element).tab('show');
    }

</script>
@yield('script')
</body>
</html>