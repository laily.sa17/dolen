@extends('website.template')
@section('title')
	EVENT - {{$config->name}}
@endsection

@section('content')
	<section>
		<br><br><br><br><br>
		<div class="container">
		<div class="col col_12_of_12">
			<div class="panel_title" jQuery>
				<div>
					<h4>Event</h4>
				</div>
				</div>
			</div>
		</div><!-- End Panel title -->
		<div class="container">
			<div class="row">
				<!-- Main content -->
				<div class="col col_12_of_12">
					<!-- Products -->
					<ul class="products clearfix">

                    @foreach($event as $result)
						<!-- Product first -->
						<li class="product ">
							<!-- Thumb -->
							<div class="item_thumb">
								<div class="thumb_hover">
									<a href="{{$result->url}}"><img src="{{$result->imagedir}}" style="object-fit: cover;height: 250px;"></a>
								</div>
							</div><!-- End Thumb -->
							<!-- Info -->
							<div class="info">
								<div class="item_meta clearfix">
								</div>
                                            <ins><span class="amount">{{$result->name}}</span></ins>
							</div><!-- End Info -->
						<!-- Product -->
						</li>
                        @endforeach
					</ul><!-- End Products -->



				</div>

            </div>
			<div class="container">
				<ul class="page-numbers">
					{!! $paginate !!}
				</ul><!-- End Pagination -->
			</div>
        </div><!-- End Thumb -->
	</section>
@endsection


@section('script')

@endsection