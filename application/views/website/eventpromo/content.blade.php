@extends('website.template')
@section('title')
	{{$event->name}}
@endsection

@section('meta')
 <meta name="author" content="{{$config->name}}">
<link rel="copyright" href="{{base_url()}}">
<meta property="og:image" content="{{$event->imagedir}}">
<meta name="keywords" content="{{$seo->keyword}} , promo">
<meta name="description" content="{{read_more($event->description,200)}}">
@endsection

@section('styles')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection

@section('content')
 <!-- Section -->
 <section>
     <div class="container">
         <div class="row">
             <!-- Main content -->
             <div class="col col_9_of_12">
                 <!-- Post -->
                 <!-- Media -->
                 <div class="entry_media">
                     <span class="meta_likes"><a href="#" data-tip="12 likes"><i class="fa fa-heart"></i></a></span>
                     <a href="{{ $event->imagedir }}" class="popup_link"><img src="{{ $event->imagedir }}" alt="{{ seo($event->name) }}" style="width:800px;"></a>
                 </div><!-- End Media -->
                 <article class="post">

                     <!-- End Full meta -->
                     <!-- Entry content -->
                     <div class="entry_content">
                         <!-- Entry title -->
                         <h1 class="entry_title">{{ $event->name }}</h1><!-- End Entry title -->

                     </div><!-- End Entry content -->
                     <div class="bottom_wrapper">
                         <!-- Entry tags -->
                         @foreach($tag as $result)
                             <div class="entry_tags">
                                 <span><i class="fa fa-tags"></i>Tags</span>
                                 <a href="{{$result->url}}">{{$result->name}}</a>
                             </div><!-- End Entry tags -->
                     @endforeach
                     <!-- Entry categories -->

                     <!-- Respond -->
                 <!--  {{-- <div id="respond">
                                <div class="panel_title">
                                    <div>
                                        <h4><a href="#">Comments</a></h4>
                                    </div>
                                </div>
                                <form id="form-comment" method="post" action="{{base_url('main/komentar/submit')}}">
                                    <p>
                                        <label>Name<span>*</span></label>
                                        <input type="text" name="name" class="form-control" placeholder="Masukkan Nama Anda" required="">

                                    </p>
                                    <p>
                                        <label>Email<span>*</span></label>
                                        <input type="email" name="email" class="form-control" placeholder="Masukkan Email Anda" required="">
                                    </p>
                                    <p>
                                        <label>Comment<span>*</span></label>
                                        <textarea class="form-control" name="comment" rows="10" placeholder="Masukkan Pesan Anda" required=""></textarea>
                                    </p>
                                    <button name="submit" type="submit" id="submit" value="Post a comment" class="btn">Post a comment</button>
                                </form>
                            </div> --}} -->
                     <div id="disqus_thread"></div>
                     <!-- End Respond -->
                 </article><!-- End Post -->

                 <div class="fb-comments"
                      data-href=data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5"
                      data-numposts="10"
                      data-width="100%"
                      data-colorscheme="light"></div>
                 <script>(function(d, s, id) {
                         var js, fjs = d.getElementsByTagName(s)[0];
                         if (d.getElementById(id)) return;
                         js = d.createElement(s); js.id = id;
                         js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1&appId=450902545437470&autoLogAppEvents=1';
                         fjs.parentNode.insertBefore(js, fjs);
                     }(document, 'script', 'facebook-jssdk'));</script>

                 <!-- Sidebar -->
             </div>

             <div class="col col_3_of_12">
                 @include('website.sidebar')
             </div>
         </div>
     </div>

 </section><!-- End Section -->
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
	$("#shareIcons").jsSocials({
		showLabel: false,
		showCount: false,
        shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
	});
</script>
@endsection