@extends('website.template')
@section('title')
	@if(isset($selected_category))
		{{$selected_category->name}} - Category
	@endif

{{-- 	@if(isset($selected_tag))
		, TAG {{$selected_tag->name}}
	@endif --}}
@endsection

@section('content')
 <!-- Section -->
    <section> <br>

        <div class="container">
            <div class="row">
                <div class="panel_title">
                    <div>
                        <h4>{{$selected_category->name}}</h4>
                    </div>
                </div><!-- End Panel title -->
                <div class="row">

                    <div class="col col_8_of_12">
                        <!-- Content slider -->
                        <div class="content_slider">
                            <ul>
                                @foreach($selected_category->images as $result)
                                <li>
                                    <a href="#"><img src="{{$result->imagedir}}" style="object-fit: cover;height: 250px;"></a>
                                </li>
                                @endforeach
                            </ul>
                        </div><!-- End Content slider -->
                    </div>

                    <div class="col col_4_of_12">
                        <div class="widget">
                            <div class="clearfix">
                                @if($enam->status == 1)

        @else
            @if(!empty($enam->video))<a href="{{$enam->link}}" data-popup="lightbox">
                <img src="{{$enam->video}}" alt="{{$enam->name}}" class="img-rounded img-preview"
                     style="object-fit: cover;width: 100%;height: 250px;">
            </a>
            @else
                <a href="{{$enam->link}}" data-popup="lightbox">
                    <img src="{{$enam->imagedir}}" alt="{{$enam->name}}" class="img-rounded img-preview"
                         style="object-fit: cover;width: 100%;height: 250px;">
                </a>
            @endif
        @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="rows">
                    @foreach($news as $result)
                    <div class="col col_4_of_12">
                        <!-- Layout post 1 -->
                        <div class="col col_12_of_12">
                        <div class="layout_post_1">
                            <div class="item_thumb">
                                <div class="thumb_hover">
                                    <a href="{{$result->url}}"><img src="{{$result->imagedir}}" alt="{{$result->name}}" style="
                                    object-fit: cover;height: 250px;"
                                    ></a>
                                </div>
                                <div class="thumb_meta">
                                   <!--contoh -->
                                    @foreach($category->where('id', $result->id_category) as $cate)
                                        <span class="category" jQuery>
                                        <a href="{{$result->url}}">{{ $cate->name }}</a></span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="item_content">
                                <h4><a href="{{$result->url }}">{{$result->name}}</a></h4>

                                    <div class="item_meta clearfix">
                                    @if($result->author == 0)
                                     <span><a href="#"><i class="fa fa-user"></i> Admin</a></span>
                                    @else
                                     <span><a href="{{ base_url('author') }}/{{ $result->penulis->username }}"><i class="fa fa-user"></i> {{ $result->penulis->name }}</a></span>
                                    @endif
                                    <span class="meta_date">{!! waktu_lalu($result->created_at) !!}</span>
                                    <span class="fa fa-eye">{!! k_view($result->view) !!}</span>
                                </div>
                                <p>{{ read_more($result->description,120) }}</p>
                            </div>
                            </div>
                        </div><!-- End Layout post 1 -->
                    </div>
                    @endforeach
                </div>
                    </div>
                <ul>
                    <div class="container">
                    {!! $pagination !!}
                    </div>
                </ul><!-- End Pagination -->
                    <!-- <div class="container">
                    <div class="fb-comments"
                         data-href=data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5"
                         data-numposts="10"
                         data-width="100%"
                         data-colorscheme="light"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1&appId=450902545437470&autoLogAppEvents=1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        </div> -->
    </section><!-- End Section -->
@endsection