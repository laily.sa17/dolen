@extends('website.template')
@section('title')
	GALLERY PHOTO - {{$config->name}}
@endsection

@section('styles')
	<!-- Styles -->
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/fontawesome.css">
	<link rel="stylesheet" href="css/weather.css">
	<link rel="stylesheet" href="css/colors.css">
	<link rel="stylesheet" href="css/typography.css">
	<link rel="stylesheet" href="css/style.css">

	<!-- Responsive -->
	<link rel="stylesheet" type="text/css" media="(max-width:768px)" href="css/responsive-0.css">
	<link rel="stylesheet" type="text/css" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
	<link rel="stylesheet" type="text/css" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
	<link rel="stylesheet" type="text/css" media="(min-width:1201px)" href="css/responsive-1200.css">
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>
	<style type="text/css">
		@media screen and (max-width: 992px) {
			.card{
				width: 100%
			}
			.row {
				position: relative;
			}

			.rows{
				display: grid;
				grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
			}

		}

		@media screen and (max-width: 768px) {
			.card{
				width: 100%;
			}
			.row {
				position: relative;
			}

		}
		@media screen and (max-width: 747px) {
			.card{
				width: 100%
			}
			.row {
				position: relative;
			}

			.rows{
				display: grid;
				grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
			}
			}

		@media screen and (max-width: 499px) {

			.card{
				width: 100%
			}
			.row {
				position: relative;
			}

			.rows{
				display: grid;
				grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
			}
		}
	</style>
@endsection

@section('script')
	<script type="text/javascript" src="js/jqueryscript.min.js"></script>
	<script type="text/javascript" src="js/jqueryuiscript.min.js"></script>
	<script type="text/javascript" src="js/easing.min.js"></script>
	<script type="text/javascript" src="js/smoothscroll.min.js"></script>
	<script type="text/javascript" src="js/magnific.min.js"></script>
	<script type="text/javascript" src="js/bxslider.min.js"></script>
	<script type="text/javascript" src="js/fitvids.min.js"></script>
	<script type="text/javascript" src="js/viewportchecker.min.js"></script>
	<script type="text/javascript" src="js/init.js"></script>
@endsection

@section('content')
	<!-- Section -->
	<section>
		<div class="container">
			<br><br><br><br>
				<div class="panel_title">
					<div>
						<h4>PHOTO</h4>
					</div>
				</div><!-- End Panel title -->
			<br>
				<div class="row">
				<div class="rows">
					@foreach($total as $result)
						<div class="col col_4_of_12">
							<div class="card">
							<figure class="wp-caption alignleft" jQuery>
								<a href="{{$result->imagedir}}" class="popup_link"><img src="{{$result->imagedir}}" alt="Image" style="object-fit: cover;height: 200px;"></a>
								<br>
								<figcaption class="wp-caption-text"><h4><a href="{{$result->url}}">{{$result->name}}</a></h4></figcaption>
							</figure>
							</div>
						</div>
					@endforeach
				</div>
				</div>
				<ul class="page-numbers">
					{!! $pagination !!}
				</ul><!-- End Pagination -->
		</div>
	</section><!-- End Section -->

@endsection
