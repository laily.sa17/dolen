@extends('website.template')
@section('title')
	Hubungi Kami
@endsection

@section('styles')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	<!-- <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style> -->
@endsection
@section('script')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
	<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/extension_blockui.js"></script>
	<script type="text/javascript">
	function initMap() {
        //map dinamais
        var uluru = {lat: {{goExplode($config->gmap_query,'&&',0)}}, lng: {{goExplode($config->gmap_query,'&&',1)}} };
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      

	$("#form-contact").submit(function(e){
		e.preventDefault();

		$.ajax({
				url: 		$("#form-contact").attr("action"),
				method: 	"POST",
				data:  		new FormData(this),
          		processData: false,
          		contentType: false,
				beforeSend: function(){
					blockMessage($('body'),'<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Please Wait , Processing','#fff');		
				}
			})
			.done(function(data){
				$('body').unblock();
				sweetAlert({
					title: 	((data.auth==true) ? 'Pesan Terkirim' : "Opps!"),
					text: 	((data.auth==true) ? data.msg : data.msg),
					type: 	((data.auth==true) ? "success" : "error"),
				},
				function(){
					if(data.auth==true){
						redirect("{{base_url('main')}}")
						return;
					}				
				})

			})
			.fail(function() {
			    $('body').unblock();
				sweetAlert({
					title: 	"Opss!",
					text: 	"Something Wrong , Please Try Again Later",
					type: 	"error",
				},
				function(){
				
				});
			 })
	})
	</script>
	<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBy_G0H8GDS521QaVAYPk_pinqKLqRdj3M&callback=initMap">
    </script> -->
@endsection

@section('content')

    <section class="omah">
        <div class="container">
            <div class="row">
                <!-- Main content -->
                <div class="col col_8_of_12">
                    <!-- Page title -->
                    <h1 class="page_title">Stay in touch</h1><!-- End Page title -->
                    <div style="width: 100%"><iframe width="100%" height="600" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q={{$config->gmap_query}}+(My%20Business%20Name)&amp;ie=UTF8&amp;t=&amp;z=15&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google Maps iframe generator</a></iframe></div><br />
                    <p>Donec viverra lectus sem, quis porta arcu dignissim sit amet. In id rutrum ipsum. Curabitur et quam scelerisque, accumsan diam vel, fringilla justo. Ut pretium diam eget quam blandit, ac euismod risus commodo. Phasellus lobortis viverra tempus. Proin non orci vehicula, cursus ante vitae, rutrum lacus. Integer sed sodales turpis, vitae sollicitudin massa. Maecenas et consectetur sapien. Mauris ante nunc, egestas non gravida a, pharetra at dui.</p>
                    <form id="form-contact" method="post" action="{{base_url('main/contact/submit')}}" >
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" placeholder="Masukkan Nama Anda" required="">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Masukkan Email Anda" required="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="subject">Subjek</label>
                            <input type="text" name="subject" class="form-control" placeholder="Subjek Pesan" required="">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="phone">Telepon</label>
                            <input type="text" name="phone" class="form-control" placeholder="Masukkan Telepon Anda" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message">Pesan</label>
                        <textarea type="text" name="message" rows="5" class="form-control" placeholder="Masukkan Pesan Anda" required=""></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane"></i> Kirim Pesan</button>
                </form>
                </div>
                <!-- Sidebar -->
                <div class="col col_4_of_12">
                    @include('website.sidebar')
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </section>
@endsection