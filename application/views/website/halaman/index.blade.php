@extends('website.template')
@section('title')
	{{$halaman->judul}} - {{$config->name}}
@endsection

@section('styles')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection

@section('content')
<!-- Section -->
    <!-- Wide slider -->
            <div class="wide_slider">
                <a href="post_single.html"><img src="{{ base_url('assets') }}/demo/1900x500.jpg" alt="Slider"></a>
            </div><!-- End Wide slider -->
            <!-- Section -->
            <section>
                <div class="container">
                    <div class="row">
                        <!-- Main content -->
                        <div class="col col_12_of_12">
                            <!-- Page title -->
                            <h1 class="page_title">Fullwidth page</h1><!-- End Page title -->
                            <figure class="wp-caption alignleft" jQuery>
                                <a href="demo/150x150.png" class="popup_link"><img src="{{ base_url('assets') }}/demo/150x150.png" alt="Image"></a>
                                <figcaption class="wp-caption-text">Click on image!</figcaption>
                            </figure><p>Maecenas purus odio, feugiat nec sapien sit amet, luctus blandit sem. Sed eu magna nec nunc euismod viverra sed non libero. Mauris mattis, metus mattis ultricies venenatis, magna elit ornare magna, eu feugiat sapien elit nec libero. Vivamus at sapien et velit fringilla dictum quis vitae ante. Mauris tempus nisl sit amet augue elementum, et finibus dolor ultricies. Nulla dignissim at lacus et elementum. Nullam nec rutrum lectus. Vivamus fermentum velit quam, quis rutrum erat congue at. Maecenas faucibus lorem vel lorem molestie, a iaculis ante commodo. Cras velit arcu, commodo et porta et, consectetur ac ante. Maecenas interdum risus lorem, ut tristique nibh dapibus at. Integer sit amet euismod velit. Vestibulum eget vehicula metus, id vehicula dui. Etiam eu malesuada nunc. Maecenas purus odio, feugiat nec sapien sit amet, luctus blandit sem. Sed eu magna nec nunc euismod viverra sed non libero. Mauris mattis, metus mattis ultricies venenatis, magna elit ornare magna, eu feugiat sapien elit nec libero. Vivamus at sapien et velit fringilla dictum quis vitae ante. Mauris tempus nisl sit amet augue elementum, et finibus dolor ultricies. Nulla dignissim at lacus et elementum. Nullam nec rutrum lectus. Vivamus fermentum velit quam, quis rutrum erat congue at. Maecenas faucibus lorem vel lorem molestie, a iaculis ante commodo. Cras velit arcu, commodo et porta et, consectetur ac ante. Maecenas interdum risus lorem, ut tristique nibh dapibus at. Integer sit amet euismod velit. Vestibulum eget vehicula metus, id vehicula dui. Etiam eu malesuada nunc. Maecenas purus odio, feugiat nec sapien sit amet, luctus blandit sem. Sed eu magna nec nunc euismod viverra sed non libero. Mauris mattis, metus mattis ultricies venenatis, magna elit ornare magna, eu feugiat sapien elit nec libero. Vivamus at sapien et velit fringilla dictum quis vitae ante.</p>
                            <blockquote>
                                <p><span>"</span>If you do what you love, you'll never work a day in your life. Phasellus eu sapien interdum ligula vulputate faucibus quis et lectus. Morbi a aliquet eros. Sed velit justo, volutpat nec mauris eu.<span>"</span></p>
                                <footer>Marc Anthony, <a href="http://www.twitter.com" target="blank">Twitter</a></footer>
                            </blockquote>
                            <p>Maecenas purus odio, feugiat nec sapien sit amet, luctus blandit sem. Sed eu magna nec nunc euismod viverra sed non libero. Mauris mattis, metus mattis ultricies venenatis, magna elit ornare magna, eu feugiat sapien elit nec libero. Vivamus at sapien et velit fringilla dictum quis vitae ante. Mauris tempus nisl sit amet augue elementum, et finibus dolor ultricies. Nulla dignissim at lacus et elementum. Nullam nec rutrum lectus. Vivamus fermentum velit quam, quis rutrum erat congue at. Maecenas faucibus lorem vel lorem molestie, a iaculis ante commodo. Cras velit arcu, commodo et porta et, consectetur ac ante. Maecenas interdum risus lorem, ut tristique nibh dapibus at. Integer sit amet euismod velit. Vestibulum eget vehicula metus, id vehicula dui. Etiam eu malesuada nunc. Maecenas purus odio, feugiat nec sapien sit amet, luctus blandit sem. Sed eu magna nec nunc euismod viverra sed non libero. Mauris mattis, metus mattis ultricies venenatis, magna elit ornare magna, eu feugiat sapien elit nec libero. Vivamus at sapien et velit fringilla dictum quis vitae ante. Mauris tempus nisl sit amet augue elementum, et finibus dolor ultricies. Nulla dignissim at lacus et elementum. Nullam nec rutrum lectus. Vivamus fermentum velit quam, quis rutrum erat congue at. Maecenas faucibus lorem vel lorem molestie, a iaculis ante commodo. Cras velit arcu, commodo et porta et, consectetur ac ante. Maecenas interdum risus lorem, ut tristique nibh dapibus at. Integer sit amet euismod velit. Vestibulum eget vehicula metus, id vehicula dui. Etiam eu malesuada nunc. Maecenas purus odio, feugiat nec sapien sit amet, luctus blandit sem. Sed eu magna nec nunc euismod viverra sed non libero. Mauris mattis, metus mattis ultricies venenatis, magna elit ornare magna, eu feugiat sapien elit nec libero. Vivamus at sapien et velit fringilla dictum quis vitae ante.</p>
                        </div><!-- End Main content -->
                        <div id="shareIcons"></div>
                    </div>
                </div>
            </section><!-- End Section -->
            </div>
        </div>
    </section>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
 $("#shareIcons").jsSocials({
    showLabel: false,
    showCount: false,
      shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
  });
</script>
@endsection