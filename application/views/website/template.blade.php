<!DOCTYPE>
<html>
<head>
    <title>@yield('title',$seo->title)</title>
    @yield('meta','<meta name="keywords" content="'.$seo->keyword.'">
    <meta name="description" content="'.$seo->description.'">
    <meta name="author" content="'.$config->name.'">
    <meta property="og:image" content="'.$seo->imagedir.'">')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="icon" href="{{$config->icondir}}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{base_url('assets')}}/css/normalize.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/fontawesome.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/weather.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/colors.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/typography.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/author.css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{base_url('assets')}}/css/style.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{base_url('assets/css/custom.css')}}">
    @yield('styles')
    <style type="text/css">
        .layout_post_1 > .item_thumb .thumb_icon > a > i {
            padding-top: 15px;
        }

        .list_posts > .post > .item_thumb .thumb_icon > a > i {
            padding-top: 9px;
        }

        .big-banner {

            width: 55%;
            margin-left: 22%;
            min-height: 20%;
            display: block;
            height: 55px;
            padding-top: 2px;
            position: fixed;
            z-index: 999;
            bottom: 0;

            color: #fff;
        }
        .img-banner-bottom{
            object-fit: cover;height: 100%;
        }

        @media (min-width: 1300px) {

            .big-banner-right {

                margin-right: 2%;
                margin-bottom: 2%;
                width: 10%;
                min-height: 70%;
                display: block;
                height: 55px;
                padding-top: 2px;
                position: fixed;
                z-index: 999;
                bottom: 0;
                right: 0;
                color: #fff;
                transition: ease all 1s;
                -webkit-transition: ease all 1s;
                -moz-transition: ease all 1s;
                -o-transition: ease all 1s;
            }

            .big-banner-left {
                margin-left: 2%;
                margin-bottom: 2%;
                width: 10%;
                min-height: 70%;
                display: block;
                height: 55px;
                padding-top: 2px;
                position: fixed;
                z-index: 999;
                bottom: 0;
                left: 0;
                color: #fff;
                transition: ease all 1s;
                -webkit-transition: ease all 1s;
                -moz-transition: ease all 1s;
                -o-transition: ease all 1s;
            }

            .big-banner-right-plus {

                margin-right: 2%;
                margin-bottom: 2%;
                width: 10%;
                min-height: 80%;
                display: block;
                height: 55px;
                padding-top: 2px;
                position: fixed;
                z-index: 999;
                bottom: 0;
                right: 0;
                color: #fff;
            }

            .big-banner-left-plus {
                margin-left: 2%;
                margin-bottom: 2%;
                width: 10%;
                min-height: 80%;
                display: block;
                height: 55px;
                padding-top: 2px;
                position: fixed;
                z-index: 999;
                bottom: 0;
                left: 0;
                color: #fff;
            }
        }

    </style>
    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" media="(max-width:768px)" href="{{asset('assets/css/responsive-0.css')}}">
    <link rel="stylesheet" type="text/css" media="(min-width:769px) and (max-width:992px)"
          href="{{asset('assets/css/responsive-768.cs')}}s">
    <link rel="stylesheet" type="text/css" media="(min-width:993px) and (max-width:1200px)"
          href="{{asset('assets/css/responsive-992.css')}}">
    <link rel="stylesheet" type="text/css" media="(min-width:1201px)"
          href="{{asset('assets/css/responsive-1200.css')}}">
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,300italic,400,400italic,700,700italic'
          rel='stylesheet' type='text/css'>
</head>
<body>
<!-- Wrapper -->
<div id="wrapper" class="wide">
    <!-- Header -->
    <header id="header" role="banner">
        <!-- Header meta -->
        <div class="header_meta">
            <div class="container">
                <div class="weather_forecast">
                    @if(!isset($_SESSION['member_auth']))
                        <a style="color: white; text-decoration: none;" href="{{base_url('member/masuk')}}">+
                            Artikel</a>
                    @else
                        <a style="color: white; text-decoration: none;" href="{{base_url('member')}}">Hy
                            , {{$_SESSION['member_name']}}</a>
                    @endif
                </div><!-- End Weather forecast -->
                <nav class="top_navigation" role="navigation">
                    <span class="top_navigation_toggle"><i class="fa fa-reorder" style="line-height: 50px;"></i></span>
                    <ul class="menu">
                        <li>
                        @foreach($menupengunjung as $result)
                            @if(count($result->submenupengunjung) !=0)
                                <li class="menu-item-has-children"><a style="text-decoration: none;"
                                                                      href="{{base_url($result->link)}}">{{$result->judul}}</a>

                                    <span class="top_sub_menu_toggle"></span>
                                    <ul class="sub-menu">
                                        @foreach($result->submenupengunjung as $menu)
                                            <li><a style="text-decoration: none;"
                                                   href="{{base_url($menu->link)}}">{{$menu->judul}}</a></li>
                                        @endforeach
                                    </ul>
                            @else
                                <li><a style="text-decoration: none;"
                                       href="{{base_url($result->link)}}">{{$result->judul}}</a>
                                    @endif
                                </li>
                                @endforeach
                                <li class="search_icon_form"><a href="#"><i class="fa fa-search"
                                                                            style="line-height:45px;"></i></a>
                                    <div class="sub-search">
                                        <form action="{{base_url('main/search')}}" method="get">
                                            <input type="search" name="q" placeholder="Search...">
                                            <input type="submit" value="Search">
                                        </form>
                                    </div>
                                </li>
                    </ul>
                </nav>
            </div>
        </div><!-- End Header meta -->
        <!-- Header main -->
        <div id="header_main" class="sticky header_main">
            <div class="container">
                <!-- Logo -->
                <div class="site_brand">
                    <a href="{{base_url()}}">
                        <img src="{{$config->logodir}}" alt="{{$config->name}}">
                    </a>
                </div><!-- End Logo -->
                <!-- Site navigation -->
                <nav class="site_navigation" role="navigation">
                    <span class="site_navigation_toggle"><i class="fa fa-reorder" style="line-height: 50px;"></i></span>
                    <ul class="menu">
                        @foreach($category as $result)
                            <li><a href="{{$result->url}}">{{$result->name}}</a></li>
                        @endforeach
                    </ul>
                </nav><!-- End Site navigation -->
            </div>
        </div><!-- End Header main -->
    </header><!-- End Header -->
@yield('content')

<!-- Footer -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col col_3_of_12">
                    <!-- Top menu -->
                    <div class="widget">
                        <div class="widget_title"><h3>Category</h3></div>
                        <div class="tb_widget_categories">
                            <ul>
                                @foreach($category as $result)
                                    <li><a href="{{$result->url}}">{{$result->name}}<span
                                                    class="p-count"> ({{count($result->blog)}})</span></a></li>
                                @endforeach
                            </ul>
                        </div>
                        </nav><!-- End Top menu -->
                    </div><!-- End Widget text widget -->
                </div>
                <div class="col col_3_of_12">
                    <!-- Widget top rated -->
                    <div class="widget">
                        <div class="widget_title"><h3>Popular</h3></div>
                        <div class="tb_widget_top_rated clearfix">
                        @foreach($popular as $blog)
                            <!-- Post item -->
                                <div class="item clearfix">
                                    <div class="item_thumb clearfix">
                                        <a href="#"><img src="{{$blog->imagedir}}" style="object-fit: cover;"
                                                         alt="Post"></a>
                                    </div>
                                    <div class="item_content">
                                        <div class="item_meta clearfix">
                                            <span class="fa fa-eye">{!! k_view($blog->view) !!}</span>
                                        </div>
                                        <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                    </div>
                                </div><!-- End Post item -->
                            @endforeach
                        </div>
                    </div><!-- End widget top rated -->
                </div>
                <div class="col col_3_of_12">
                    <!-- Widget recent posts -->
                    <div class="widget">
                        <div class="widget_title"><h3>New post</h3></div>
                        <div class="tb_widget_recent_list clearfix">
                        @foreach($terbaru as $blog)
                            <!-- Post item -->
                                <div class="item clearfix">
                                    <div class="item_thumb">
                                        <div class="thumb_hover">
                                            <a href="{{$blog->url}}"><img src="{{$blog->imagedir}}">
                                            </a>
                                        </div>
                                    </div>


                                    <div class="item_content">
                                        <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                        <div class="item_meta clearfix">
                                            @if($blog->author == 0)
                                                <span><a style="color: #00acedff" href="#"><i class="fa fa-user"></i> Admin</a></span>
                                            @else
                                                <span><a style="color: #00acedff"
                                                         href="{{ base_url('author') }}/{{ $blog->penulis->username }}"><i
                                                                class="fa fa-user"></i> {{ $blog->penulis->name }}</a></span>
                                            @endif

                                            <span class="meta_date">{!! waktu_lalu($blog->created_at) !!}</span>
                                        </div>
                                    </div>

                                </div><!-- End Post item -->
                            @endforeach
                        </div><!-- End Widget recent posts -->
                    </div>
                </div>
                <div class="col col_3_of_12">
                    <div class="widget">
                        <div class="widget_title"><h3>Socialize</h3></div>
                        <div class="tb_widget_socialize clearfix">
                            @foreach($sosmed as $result)
                                <a href="{{$result->url}}" target="_blank" class="icon {{$result->type}}">
                                    <div class="symbol" style="border-radius: 100%"><i class="fa fa-{{$result->type}}"
                                                                                       style="line-height: 50px; "></i>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- End Footer -->

    <!-- Copyright -->
    <div id="copyright" role="contentinfo">
        <div class="container">
            <p>&copy; 2017 Dolen All rights reserved. <a href="#" target="_blank">Jasa Pembuatan Website</a></p>
        </div>
    </div><!-- End Copyright -->
</div>
<!---------------------------------------------- BANNER IKLAN ------------------------------------------->


<div class="banner-side">
    <div class="col-xs-12 banner-text text-center">
        <div class="big-banner-left">
            <button id="close-left" style="margin-right: 1%; color: white; background-color: #0b0b0b " type="button"
                    class="close" data-dismiss="modal"
                    aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            @if($lima->status == 1)
                <img src="http://stackdigital.com.au/wp-content/uploads/2018/05/skyscraperad.gif" alt="">
            @else
                @if(!empty($lima->video))<a href="{{$lima->link}}">
                    <img src="{{$lima->video}}" alt="{{$lima->name}}" style="height: 95%">
                </a>
                @else
                    <a href="{{$lima->link}}">
                        <img src="{{$lima->imagedir}}" alt="{{$lima->name}}" style="height: 95%">
                    </a>
                @endif
            <!--  alt='Join adnow' -->
            @endif

        </div>
    </div>
    <div class="col-xs-12 banner-text text-center">
        <div class="big-banner-right">
            <button id="close-right" style="margin-right: 1%; color: white; background-color: #0b0b0b" type="button"
                    class="close" data-dismiss="modal"
                    aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @if($empat->status == 1)
                <img src="http://stackdigital.com.au/wp-content/uploads/2018/05/skyscraperad.gif" alt="">
            @else
                @if(!empty($empat->video))<a href="{{$empat->link}}">
                    <img src="{{$empat->video}}" alt="{{$empat->name}}" style="height: 95%">
                </a>
                @else
                    <a href="{{$empat->link}}">
                        <img src="{{$empat->imagedir}}" alt="{{$empat->name}}" style="height: 95%">
                    </a>
                @endif
            @endif

        </div>
    </div>

</div>
<div class="col-xs-12 banner-text text-center">
    <div class="big-banner">
        <button id="close-bottom" style="margin-left: 2%; background-color: #0b0b0b; color: white" type="button"
                class="close" data-dismiss="modal"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        @if($tiga->status == 1)
            <img src="http://www.quikr.lk/images/top-ad-banner1.png" alt="">
        @else
            @if(!empty($tiga->video))<a href="{{$tiga->link}}">
                <img src="{{$tiga->video}}" alt="{{$tiga->name}}">
            </a>
            @else
                <a href="{{$tiga->link}}">
                    <img src="{{$tiga->imagedir}}" alt="{{$tiga->name}}">
                </a>
            @endif
        @endif

    </div>
</div>


<!--  -->
<script type="text/javascript" src="{{base_url('assets')}}/js/jqueryscript.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/jqueryuiscript.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/easing.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/smoothscroll.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/magnific.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/bxslider.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/fitvids.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/viewportchecker.min.js"></script>
<script type="text/javascript" src="{{base_url('assets')}}/js/init.js"></script>
<script type="text/javascript">
    $('#close-bottom').click(function () {
        $('#close-bottom').fadeOut();
        $('.big-banner').fadeOut();
        $('#modaliklan').modal('hide');
    });
    $('#close-left').click(function () {
        $('#close-left').fadeOut();
        $('.big-banner-left').fadeOut();
        $('#modaliklan').modal('hide');
    });
    $('#close-right').click(function () {
        $('#close-right').fadeOut();
        $('.big-banner-right').fadeOut();
        $('#modaliklan').modal('hide');
    });

    $(window).scroll(function () {

        var scroll = $(window).scrollTop();

        if (scroll >= 40) {
            $(".big-banner-left").addClass("big-banner-left-plus");
            $(".big-banner-right").addClass("big-banner-right-plus");
        } else {
            $(".big-banner-left").removeClass("big-banner-left-plus")
            $(".big-banner-right").removeClass("big-banner-right-plus")
        }
    });
</script>
@yield('script')
</body>

</div>
</footer><!-- End Footer -->
<!-- Copyright -->

</html>