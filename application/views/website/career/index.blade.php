@extends('website.template')
@section('title')
    CAREER
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/core.css"/>
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/components.css"/>
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/colors.css"/>
    <link rel="stylesheet" type="text/css" href="{{ base_url() }}admin_assets/css/icons/icomoon/styles.css"/>
@endsection
@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript"
            src="{{base_url()}}admin_assets/js/plugins/uploaders/fileinput.min.js"></script>
    <script type="text/javascript"
            src="{{base_url()}}admin_assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/form_layouts.js"></script>

    <script type="text/javascript">
        var editorsmall = false;
    </script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/editor_ckeditor.js"></script>

    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/uploader_bootstrap.js"></script>
    <script type="text/javascript"
            src="{{base_url()}}admin_assets/js/plugins/forms/styling/switch.min.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="{{base_url()}}admin_assets/js/pages/extension_blockui.js"></script>
    <script type="text/javascript">
        $(".select-search").select2({
            placeholder: "Pilih Tag Blog",
            allowClear: true
        });

        // $(".image-product-upload").change(function (e) {
        //     var input = this;
        //     var element = $(this).parents('.col-md-4').find('img');
        //     var href = $(this).parents('.col-md-4').find('a[rel="gallery"]');
        //
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //
        //         reader.onload = function (e) {
        //             jQuery(element).attr('src', e.target.result);
        //             jQuery(href).attr('href', e.target.result);
        //         }
        //         reader.readAsDataURL(input.files[0]);
        //     }
        // })
        $("#form-career").submit(function (e) {
            e.preventDefault();
            var formData = new FormData($("#form-career")[0]);

            $.ajax({
                url: $("#form-career").attr('action'),
                method: "POST",
                data: new FormData(this),
                processData: false,
                contentType: false,
                beforeSend: function () {
                    blockMessage($('#form-career'), 'Please Wait', '#fff');
                }
            })
                .done(function (data) {
                    $('#form-career').unblock();
                    sweetAlert({
                            title: ((data.auth == false) ? "Opps!" : 'Sukses'),
                            text: data.msg,
                            type: ((data.auth == false) ? "error" : "success"),
                        },
                        function () {
                            if (data.auth != false) {
                                redirect("{{base_url('main/career')}}");
                                return;
                            }
                        });
                })
                .fail(function () {
                    $('#form-tag').unblock();
                    sweetAlert({
                        title: "Opss!",
                        text: "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                        type: "error",
                    });
                })

        })

    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBy_G0H8GDS521QaVAYPk_pinqKLqRdj3M&callback=initMap">
    </script>
@endsection

@section('content')

    <section class="omah">
        <div class="container">
            <div class="row">
                <!-- Main content -->
                <div class="col col_8_of_12">
                    <!-- Page title -->
                    <h1 class="page_title">Career</h1><!-- End Page title -->
                    <form id="form-career" action="{{base_url('main/career')}}">
                        <div class="col-lg-12">
                            <p>
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Your Name"
                                       required="">
                            </p>
                        </div>
                        <div class="col-lg-12">
                            <p>
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Enter Your Email"
                                       required="">
                            </p>
                        </div>


                        <div class="col-lg-7">
                            <p>
                                <label>Phone Number</label>
                                <input type="number" name="phone" class="form-control" placeholder="Enter Your Email"
                                       required="">
                            </p>
                        </div>
                        <div class="col-lg-5">
                            <select class="select-search" name="job" style="margin-top: 11%">
                                <option value="">Job Position</option>
                                @foreach($job as $result)
                                    <option value="{{$result->id}}">{{$result->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-12">
                            <p>
                                <label>File CV</label>
                            <div>
                                <div id="wrap-image" style="margin-top: -2%">
                                    <input type="file" name="file_cv" accept="file/*"
                                           class="file-styled image-product-upload">
                                </div>
                            </div>
                            </p>
                        </div>
                        <div class="col-lg-12">
                            <p>
                                <button type="submit" class="btn btn-primary"> Submit</button>
                                <!--<a href="#" class="btn"><i class="fa fa-paper-plane"></i> Submit</a>-->
                            </p>
                        </div>
                    </form>
                </div>
                <!-- Sidebar -->
                <div class="col col_3_of_12">
                    @include('website.sidebar')
                </div>
                <!-- End Sidebar -->
            </div>
        </div><!-- End Main content -->
        

        
    </section><!-- End Section -->
@endsection