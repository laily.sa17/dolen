@extends('website.template')
@section('title')
	OPPS PAGE NOT FOUND!!!
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <!-- Main content -->
                <div class="col col_12_of_12">
                    <!-- Page title -->
                    <h1 class="page_title">Page not found</h1><!-- End Page title -->
                    <!-- 404 Page -->
                    <div class="page_404">
                        <h3>404</h3>
                        <h4>Something went terribly wrong...</h4>
                        <p>But don't worry, it can happen to the best of us - and it just happen to you!<br>
                        You can search something else or read this text one more time.</p>
                        <form action="{{base_url('main/search')}}">
                            <input type="text" placeholder="Type and press enter..." name="q">

                        </form>
                    </div><!-- End 404 Page -->
                </div><!-- End Main content -->
            </div>
        </div>
    </section><!-- End Section -->
@endsection


@section('script')

@endsection