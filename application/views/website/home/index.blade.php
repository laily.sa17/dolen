@extends('website.template')
@section('title')
    {{$config->name}}
@endsection

@section('styles')

@endsection

@section('script')
@endsection
@section('content')

    <!-- Section -->
    <section class="omah">
        <div class="container">
            <div class="row">
                <!-- Main content -->
                <div class="col col_12_of_12">
                    <!-- Content slider -->
                    <div class="content_slider">
                        <ul>
                            @foreach($slider as $sldr)
                                <li>
                                    <a href="{{$sldr->url}}"><img src="{{$sldr->imagedir}}" alt="{{$sldr->name}}"></a>
                                    <div class="slider_caption">
                                        <div class="thumb_meta">
                                            <span class="category" jQuery><a href="{{$sldr->url}}"> </a></span>
                                            <span class="comments"></span>
                                        </div>
                                        <div class="thumb_link">
                                            <h3><a href="{{$sldr->url}}">{{$sldr->name}}</a></h3>
                                        </div>
                                    </div>
                                </li><!-- End Item -->
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col col_9_of_12">
                    <div class="panel_title">
                        <div>

                            <h4><a href="#">Trending topik lagi</a></h4>
                        </div>
                    </div><!-- End Panel title -->
                    <div class="row">

                        <div class="col col_12_of_12">
                            <div class="multipack clearfix">
                                <!-- Layout post 1 -->
                                <div class="layout_post_1">
                                    <div class="item_thumb">
                                        <div class="thumb_icon">
                                            <a href="{{ $news1->url }}" jQuery><i class="fa fa-copy"></i></a>
                                        </div>
                                        <div class="thumb_hover">
                                            <a href="{{$news1->category->url}}">
                                                <img src="{{$news1->imagedir}}" alt="{{ $news1->name }}"></a>
                                        </div>
                                        <div class="thumb_meta">
                                            <span class="category" jQuery>
                                            <a href="{{$news1->category->url}}">{{$news1->category->name}}</a></span>
                                            <span class="comments"></span>
                                        </div>
                                    </div>


                                <!-- <span><a href="{{ base_url('author') }}/{{ $news->penulis->username }}"><i class="fa fa-user"></i> {{ $news->penulis->name }}</a></span> -->
                                    <div class="item_content">
                                        <h4><a href="{{ $news1->url }}">{{ $news1->name }}</a></h4>
                                        <div class="item_meta clearfix">
                                            <span><a href="{{ base_url('author') }}/{{ $news1->penulis->username }}"><i
                                                            class="fa fa-user"></i> {{ $news1->penulis->name }}</a></span>
                                            <span class="meta_date">{!! waktu_lalu($news1->created_at) !!}</span>
                                            <span class="fa fa-eye">{!! k_view($news1->view) !!}</span>
                                        </div>
                                    </div>
                                </div><!-- End Layout post 1 -->
                                <!-- Post lists -->
                                <div class="list_posts">
                                    <!-- Post -->
                                    @foreach($trending as $result)
                                        <div class="post clearfix">
                                            <div class="item_thumb">
                                                <div class="thumb_icon">
                                                    <a href="{{ $result->url }}" jQuery><i class="fa fa-copy"></i></a>
                                                </div>
                                                <div class="thumb_hover">
                                                    <a href="{{ $result->url }}"><img class="newsPict"
                                                                                      src="{{ $result->imagedir }}"
                                                                                      alt="{{ $result->name }}"></a>
                                                </div>
                                            </div>
                                            <div class="item_content">
                                                <h4><a href="{{ $result->url }}">{{ $result->name }}</a></h4>
                                                <div class="item_meta clearfix">
                                                    @if($result->author == 0)
                                                        <span><a style="color: #00acedff" href="{{base_url('main/admin')}}"><i
                                                                        class="fa fa-user"></i> Admin</a></span>
                                                    @else
                                                        <span><a style="color: #00acedff"
                                                                 href="{{ base_url('author') }}/{{ $result->penulis->username }}"><i
                                                                        class="fa fa-user"></i> {{ $result->penulis->name }}</a></span>
                                                    @endif
                                                    <span class="meta_date">{!! waktu_lalu($result->created_at) !!}</span>
                                                    <span class="fa fa-eye">{!! k_view($result->view) !!}</span>
                                                </div>
                                            </div>
                                        </div><!-- End Post -->
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel_title" jQuery>
                        <div>
                            <h4><a href="#">Terbaru</a></h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="rows">
                            @foreach($terbaru as $blog)
                                <div class="col col_6_of_12">
                                    <div class="top_review">
                                        <div class="item_content">
                                            <a class="hover_effect" href="{{$blog->url}}">
                                                <img src="{{$blog->imagedir}}" alt="{{ $blog->name }}"
                                                     style="object-fit: cover;height: 250px;">
                                            </a>
                                            <h4><a href="{{$blog->category->url}}">
                                                    <span class="format" >{{$blog->category->name}}</span></a>
                                                <a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                        </div>
                                        {!! read_more($blog->description,80) !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- Panel title -->
                    <div class="panel_title">
                        <div>
                            <h4><a href="#">Popular</a></h4>
                        </div>
                    </div><!-- End Panel title -->
                    <!-- Layout post 2 -->
                    <div class="row">
                        <div class="rowsPopular">

                            <!-- Layout post 1 -->
                            @foreach($popular as $blog)
                                <div class="col col_12_of_12">
                                    <div class="card">
                                        <div class="layout_post_2 clearfix">
                                            <div class="item_thumb">
                                                <div class="thumb_hover">
                                                    <a href="{{$blog->url}}">
                                                        <img src="{{$blog->imagedir}}"
                                                             style="object-fit: cover;height: 210px;"
                                                             alt="{{ $blog->name }}"></a>
                                                </div>
                                                <div class="thumb_meta">
                                                    @foreach($category->where('id', $blog->id_category) as $cate)
                                                        <span class="category" jQuery><a
                                                                    href="{{$blog->category->url}}">{{ $cate->name }}</a></span>
                                                    @endforeach
                                                    <span class="comments">
                                        </span>
                                                </div>

                                            </div>
                                            <div class="item_content">
                                                <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                                <p>{!! read_more($blog->description,200) !!}</p>
                                                <div class="item_meta clearfix">
                                                    <span><a href="{{ base_url('author') }}/{{ $blog->penulis->username }}"><i
                                                                    class="fa fa-user"></i> {{ $blog->penulis->name }}</a></span>
                                                    <span class="meta_date">{!! waktu_lalu($blog->created_at) !!}</span>
                                                    <span class="fa fa-eye">{!! k_view($blog->view) !!}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                        <!-- End Layout post 1 -->

                        </div>
                    </div>
                </div><!-- End Main content -->
                <!-- Sidebar -->
                <div class="col col_3_of_12">
                    @include('website.sidebar')
                </div>

            </div>
        </div>
        <div>
            @if($satu->status == 1)

            @else
                @if(!empty($satu->video))<a href="{{$satu->link}}">
                    <img src="{{$satu->video}}" alt="{{$satu->name}}" class="img-rounded img-preview"
                         style="width: 75%;height: 269px; margin-left: 12.5%; margin-right: 12.5%">
                </a>
                @else
                    <a href="{{$satu->link}}">
                        <img src="{{$satu->imagedir}}" alt="{{$satu->name}}" class="img-rounded img-preview"
                             style="width: 75%;height: 269px;  margin-left: 12.5%; margin-right: 12.5%">
                    </a>
                @endif
            @endif
        </div>
        <br>
        <br>
        <br>
        <!-- Panel title -->
        <div class="container">
            <div class="panel_title">
                <div>
                    <h4><a href="blog.html">Pilihan </a></h4>
                </div>
            </div><!-- End Panel title -->
            <div class="row">
                <div class="rows">
                    @foreach($pilihan as $blog)
                        <div class="col col_4_of_12">
                            <!-- Layout post 1 -->
                            <div class="card">
                                <div class="layout_post_1">
                                    <div class="item_thumb">
                                        <div class="thumb_hover">
                                            <a href="{{$blog->url}}"><img src="{{$blog->imagedir}}"
                                                                          style="object-fit: cover;height: 250px;"></a>
                                        </div>
                                        <div class="thumb_meta">
                                            @foreach($category->where('id', $blog->id_category) as $cate)
                                                <span class="category" jQuery><a
                                                            href="{{$blog->category->url}}">{{ $cate->name }}</a></span>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="item_content">
                                        <h4><a href="{{$blog->url}}">{{$blog->name}}</a></h4>
                                        <p>{!! read_more($blog->description,200) !!}</p>
                                        <div class="item_meta clearfix">
                                            <span><a href="{{ base_url('author') }}/{{ $blog->penulis->username }}"><i
                                                            class="fa fa-user"></i> {{ $blog->penulis->name }}</a></span>
                                            <span class="meta_date">{!! waktu_lalu($blog->created_at) !!}</span>
                                            <span class="fa fa-eye">{!! k_view($blog->view) !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        </div>
    </section><!-- End Section -->
@endsection
