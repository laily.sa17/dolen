@extends('website.template')
@section('title')
  Lupa Password?   - {{$config->name}}
@endsection

@section('content')
    <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-main">
       <div class="cart wow bounceInUp animated">
          <div class="gap-sm"></div>
          <div class="row">
            <div class="col-sm-6 col-sm-push-2">
              <div class="box-password" style="margin-top: 20px;">
                <form id="form-lupa" method="post">
                  <div class="header">
                    <h3 style="color: orange; border-bottom:1px solid #eaeaea;margin: 0 0 15px 0;
                      padding: 25px 10px;">Lupa Password ?</h3>
                  </div>
                  Anda Akan Menerima Tautan Untuk Membuka Password Baru<br><br><br>
                  <div class="gap-sm"></div>
                  <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email Anda" required>
                  </div>
                  <div class="form-group">
                    <center>
                      <button type="submit" class="btn btn-danger btn-block">Dapatkan Passowrd Baru</button>
                    </center>
                  <div class="gap-xs"></div>
                    <span class="help-block text-center" style="float: left;">
                      <span style="font-size: 13px; margin-left:135px;" >Sudah Ingat Password ?</span>&nbsp;&nbsp;<a href="{{base_url('member/masuk')}}" style="font-size: 13px;" class="text-primary">Login</a>
                    </span>
                  </div>
                </form>
            </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('styles')
<link href="{{base_url()}}admin_assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
<style type="text/css">
.jGrowl.top-center {
    top: 170px;
    color: white;
}
</style>
@endsection

@section('script')
<script type="text/javascript" src="{{base_url()}}admin_assets/js/aksa/aksa-js.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/notifications/jgrowl.min.js"></script>

<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/loaders/blockui.min.js"></script>
<script type="text/javascript" src="{{base_url()}}admin_assets/js/plugins/notifications/pnotify.min.js"></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoadGoogleCallback" async defer></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
  $( "input[type=email]" ).focus();
  $("#form-lupa").submit(function(e){
  e.preventDefault();
  var block = $("#form-lupa");
  
  $.ajax({
    url : "{{ base_url('member/lostpassword/submit') }}",
    method : 'POST',
    data : $("#form-lupa").serialize(),
    beforeSend:function(){
      showBlock($(block),'<i class="fa fa-2x fa-spinner fa-spin"></i>','#fff');   
    }
  })
  .done(function(data){
    if (data.auth==false){
      ShowNotif('Lupa Password Gagal!',data.msg,'top-center','bg-danger');
      $(block).find("input[type=email]").val('');
      $(block).unblock();
      $("input[type=email]").focus();
      return;
    }
    ShowNotif('Lupa Password Berhasil!',data.msg,'top-center','bg-success');
    var go  = setTimeout(function(){
      redirect('{{ base_url('member') }}');
    },2000);

  })
})
</script>
@endsection