@extends('website.template')
@section('title')
	BANNER - {{$config->name}}

{{-- 	@if(isset($selected_tag))
		, TAG {{$selected_tag->name}}
	@endif --}}
@endsection

@section('styles')
    <!-- Styles -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/fontawesome.css">
    <link rel="stylesheet" href="css/weather.css">
    <link rel="stylesheet" href="css/colors.css">
    <link rel="stylesheet" href="css/typography.css">
    <link rel="stylesheet" href="css/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" media="(max-width:768px)" href="css/responsive-0.css">
    <link rel="stylesheet" type="text/css" media="(min-width:769px) and (max-width:992px)" href="css/responsive-768.css">
    <link rel="stylesheet" type="text/css" media="(min-width:993px) and (max-width:1200px)" href="css/responsive-992.css">
    <link rel="stylesheet" type="text/css" media="(min-width:1201px)" href="css/responsive-1200.css">
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:300,300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <style type="text/css">
        @media screen and (max-width: 992px) {
            .card{
                width: 100%
            }
            .row {
                position: relative;
            }

            .rows{
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
            }

        }

        @media screen and (max-width: 768px) {
            .card{
                width: 100%;
            }
            .row {
                position: relative;
            }

        }
        @media screen and (max-width: 747px) {
            .card{
                width: 100%
            }
            .row {
                position: relative;
            }

            .rows{
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
            }
            }

        @media screen and (max-width: 499px) {

            .card{
                width: 100%
            }
            .row {
                position: relative;
            }

            .rows{
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
            }
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript" src="js/jqueryscript.min.js"></script>
    <script type="text/javascript" src="js/jqueryuiscript.min.js"></script>
    <script type="text/javascript" src="js/easing.min.js"></script>
    <script type="text/javascript" src="js/smoothscroll.min.js"></script>
    <script type="text/javascript" src="js/magnific.min.js"></script>
    <script type="text/javascript" src="js/bxslider.min.js"></script>
    <script type="text/javascript" src="js/fitvids.min.js"></script>
    <script type="text/javascript" src="js/viewportchecker.min.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
@endsection

@section('content')
    <!-- Section -->
    <section>
        <div class="container">
            <br><br><div class="panel_title">
                <div>
                    <h4>VIDEO</h4>
                </div>
            </div><!-- End Panel title -->
            <br>
            <div class="row">
                <div class="rows">
                    @foreach($total as $result)
  @if(!empty($result->video))
                                                <div class="col col_6_of_12">
                            <div class="card">
                                <div class="top_review" style="min-height: 250px">
                                    <div class="item_content">
                                       
                                        <a class="hover_effect" href="{{$result->video}}" rel="group"
                                           title="{{$result->title}}">
                                            <img src="{{$result->video}}" style="object-fit: cover;height: 200px;">
                                        </a>
                                        <br>
                                        <h4><a href="{{$result->url}}">{{$result->name}}</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @else
                        @endif
                    @endforeach
                </div>
            </div>
             <br><br><div class="panel_title">
                <div>
                    <h4>PICTURE</h4>
                </div>
            </div><!-- End Panel title -->
            <br>
            <div class="row">
                <div class="rows">
                    @foreach($total as $result)
  @if(!empty($result->image))
                                                <div class="col col_6_of_12">
                            <div class="card">
                                <div class="top_review" style="min-height: 250px">
                                    <div class="item_content">
                                       
                                        <a class="hover_effect" href="{{$result->imagedir}}" rel="group"
                                           title="{{$result->title}}">
<<<<<<< Updated upstream
                                            <img src="{{$result->imagedir}}" style="object-fit: cover;height: 200px;">
                                        </a>
=======
                                            <img src="{{$result->imagedir}}" style="object-fit: cover;height: 350px;"/>


>>>>>>> Stashed changes
                                        <br>
                                        <h4><a href="{{$result->url}}">{{$result->name}}</a></h4>

                                    </div>
                                </div>
                            </div>
                        </div>

                        @else
                        @endif
                    @endforeach
                </div>
            </div>
                <ul class="page-numbers">
                    {!! $pagination !!}
                </ul><!-- End Pagination -->
        </div>
    </section><!-- End Section -->

@endsection