@extends('website.template')
@section('title')
    GALLERY VIDEO - {{$config->name}}
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.css">
    <style type="text/css">
        .hover_effect {
            color: #fff;
            position: relative;
            display: block
        }
        @media screen and (max-width: 992px) {
            .card {
                width: 100%
            }

            .row {
                position: relative;
            }

            .rows {
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
            }

        }

        @media screen and (max-width: 768px) {
            .card {
                width: 100%;
            }

            .row {
                position: relative;
            }

        }

        @media screen and (max-width: 747px) {
            .card {
                width: 100%
            }

            .row {
                position: relative;
            }

            .rows {
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
            }
        }

        @media screen and (max-width: 499px) {

            .card {
                width: 100%
            }

            .row {
                position: relative;
            }

            .rows {
                display: grid;
                grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
            }
        }
    </style>
@endsection

@section('script')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js"></script>
    <script type="text/javascript">
        jQuery("a[rel=group]").click(function () {
            jQuery.fancybox({
                'padding': 0,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'title': this.title,
                'width': 680,
                'height': 495,
                'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                'type': 'swf',
                'swf': {
                    'wmode': 'transparent',
                    'allowfullscreen': 'true'
                }
            });

            return false;
        });
    </script>
@endsection

@section('content')
    <!-- Section -->
    <section>
        <div class="container">
            <br><br>
            <div class="panel_title">
                <div>
                    <h4>VIDEO</h4>
                </div>
            </div><!-- End Panel title -->
            <br>
            <div class="row">
                <div class="rows">
                    @foreach($total as $result)

                        <div class="col col_4_of_12">
                            <div class="card">
                                <div class="top_review" style="min-height: 250px">
                                    <div class="item_content">
                                        <a class="hover_effect" href="{{$result->videodir}}" rel="group"
                                           title="{{$result->title}}">
                                            <img src="{{youtube_preview($result->videodir)}}">
                                        </a>
                                        <br>
                                        <h4><a href="{{$result->url}}">{{$result->name}}</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <ul class="page-numbers">
                {!! $pagination !!}
            </ul><!-- End Pagination -->

        </div>
    </section><!-- End Section -->
@endsection