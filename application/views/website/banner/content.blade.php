@extends('website.template')
@section('title')
    {{$pict->name}}
@endsection

{{--@section('meta')--}}
    {{--<meta name="author" content="{{$config->name}}">--}}
    {{--<link rel="copyright" href="{{base_url()}}">--}}
    {{--<meta property="og:image" content="{{$news->imagedir}}">--}}
    {{--<meta name="keywords" content="{{$news->category->name }} , {{$tag_txt}}">--}}
    {{--<meta name="description" content="{{read_more($news->description,170)}}">--}}
{{--@endsection--}}

@section('styles')

    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
    <style type="text/css">
        .btn {
            color: #fff;
            border: 0;
            background-color: #f85050;
            display: inline-block;
            padding: 6px 16px;
            margin: 0 5px 10px 0;
            font-size: 14px;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            position: relative;
        }
    </style>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <!-- Main content -->
                <div class="col col_9_of_12">
                    <!-- Post -->
                    <!-- Media -->
                    <div class="entry_media">
                        <span class="meta_likes"><a href="#" data-tip="12 likes"><i class="fa fa-heart"></i></a></span>
                               @if(!empty($pict->video))<a href="{{$pict->url}}" data-popup="lightbox"><center>
                                    <img src="{{$pict->video}}" alt="{{$pict->name}}" class="img-rounded img-preview" >
                                     </center>
                                </a>
                                @else
                                <a href="{{$pict->url}}" data-popup="lightbox">
                                    <center> 
                                    <img src="{{$pict->imagedir}}" alt="{{$pict->name}}" class="img-rounded img-preview" >
                                     </center>
                                </a>
                                @endif
                    </div><!-- End Media -->
                    <!-- Full meta -->
                    <div class="full_meta clearfix">
                        <span class="meta_format"><i class="fa fa-file-text" style="line-height:40px;"></i></span>
                        <span class="meta_date">{!! waktu_lalu($pict->created_at) !!}</span>
                        <span class="fa fa-eye" style="line-height:40px;">&nbsp;
                            {!! k_view($pict->view) !!}</span>
                    </div>
                    <article class="post">
                        <!-- Entry content -->
                        <div class="entry_content">
                            <!-- Entry title -->
                            <h1 class="entry_title">{{ $pict->name }}</h1><!-- End Entry title -->
                            <p class="dropcap">{!! $pict->description !!}</p>
                        </div><!-- End Entry content -->
                        <div class="bottom_wrapper">
                            <!-- Entry tags -->
                        <div id="disqus_thread"></div>
                        <!-- End Respond -->
                        </div>
                    </article><!-- End Post -->

                    <div class="fb-comments"
                         data-href=data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5"
                         data-numposts="10"
                         data-width="100%"
                         data-colorscheme="light"></div>
                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1&appId=450902545437470&autoLogAppEvents=1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>

                    <!-- Sidebar -->
                </div>

                <div class="col col_3_of_12">
                    @include('website.sidebar')
                </div>
            </div>
        </div>

    </section><!-- End Section -->
@endsection

@section('script')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    <script>
        $("#shareIcons").jsSocials({
            showLabel: false,
            showCount: false,
            shares: ["twitter", "facebook", "whatsapp","googleplus","line"]
        });

        var disqus_config = function () {
            this.page.url = '{{$news->url}}';  // Replace PAGE_URL with your page's canonical URL variable
            this.page.identifier = {{$news->id}}; \
        };
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
            s.src = '//hayyu.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    <script id="dsq-count-scr" src="//hayyu.disqus.com/count.js" async></script>
@endsection