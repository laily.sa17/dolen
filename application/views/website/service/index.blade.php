@extends('website.template')
@section('title')
	OUR SERVICE - {{$config->name}}
@endsection

@section('content')
	<!-- Section -->
            <section>
                <div class="container">
                    <div class="row">
		                <div class="panel_title">
		                    <div>
		                        <h4><a href="#">Pantai</a></h4>
		                    </div>
		                </div><!-- End Panel title -->
                    	<div class="row">

		                    <div class="col col_8_of_12">
		                        <!-- Content slider -->
		                        <div class="content_slider">
		                            <ul>
		                                <li>
		                                    <a href="post_single.html"><img src="demo/content_slider/8.jpg" alt="Slider"></a>
		                                    <div class="slider_caption">
		                                        <div class="thumb_meta">
		                                            <span class="category" jQuery><a href="blog.html">Entertainment</a></span>
		                                            <span class="comments"><a href="post_single.html">358 Comments</a></span>
		                                        </div>
		                                        <div class="thumb_link">
		                                            <h3><a href="post_single.html">Suspendisse porta quam eget nibh rhoncus eget ornare urna varius ed ut mauris augu uspendisse viverra elit libero</a></h3>
		                                        </div>
		                                    </div>
		                                </li><!-- End Item -->
		                                <!-- Item -->
		                                <li>
		                                    <a href="post_single.html"><img src="demo/content_slider/7.jpg" alt="Slider"></a>
		                                    <div class="slider_caption">
		                                        <div class="thumb_meta">
		                                            <span class="category" jQuery><a href="blog.html">Technology</a></span>
		                                            <span class="comments"><a href="post_single.html">358 Comments</a></span>
		                                        </div>
		                                        <div class="thumb_link">
		                                            <h3><a href="post_single.html">Suspendisse porta quam eget nibh rhoncus eget ornare urna varius ed ut mauris augu uspendisse viverra elit libero</a></h3>
		                                        </div>
		                                    </div>
		                                </li><!-- End Item -->
		                                <!-- Item -->
		                                <li>
		                                    <a href="post_single.html"><img src="demo/content_slider/5.jpg" alt="Slider"></a>
		                                    <div class="slider_caption">
		                                        <div class="thumb_meta">
		                                            <span class="category" jQuery><a href="blog.html">Sport</a></span>
		                                            <span class="comments"><a href="post_single.html">358 Comments</a></span>
		                                        </div>
		                                        <div class="thumb_link">
		                                            <h3><a href="post_single.html">Suspendisse porta quam eget nibh rhoncus eget ornare urna varius ed ut mauris augu uspendisse viverra elit libero</a></h3>
		                                        </div>
		                                    </div>
		                                </li><!-- End Item -->
		                            </ul>
		                        </div><!-- End Content slider -->
		                    </div>

		                    <div class="col col_4_of_12">
		                        <div class="widget">
		                            <div class="clearfix">
		                                <a href="#" target="_blank">
		                                    <img src="demo/500x500/12.jpg" alt="Banner">
		                                </a>
		                            </div>
		                        </div>
		                    </div>
	                    </div>

		                <div class="row">
		                    <div class="col col_4_of_12">
		                        <!-- Layout post 1 -->
		                        <div class="layout_post_1">
		                            <div class="item_thumb">
		                                <div class="thumb_icon">
		                                    <a href="post_single.html" jQuery><i class="fa fa-copy"></i></a>
		                                </div>
		                                <div class="thumb_hover">
		                                    <a href="post_single.html"><img src="demo/500x500/12.jpg" alt="Post"></a>
		                                </div>
		                                <div class="thumb_meta">
		                                    <span class="category" jQuery><a href="blog.html">Football</a></span>
		                                    <span class="comments"><a href="post_single.html">15 Comments</a></span>
		                                </div>
		                            </div>
		                            <div class="item_content">
		                                <h4><a href="post_single.html">Maecenas tempor volutpat commodo uspendisse potenti</a></h4>
		                                <div class="item_meta clearfix">
		                                    <span class="meta_date">October 3, 2014</span>
		                                    <span class="meta_likes"><a href="#">29</a></span>
		                                </div>
		                                <p>In et consequat nisi, at arcu. In hac habitasse platea dictumst. Fusce vestibulum tincidunt magna vitae scelerisque. Pellentesque venenatis velit ut feugia [...]</p>
		                            </div>
		                        </div><!-- End Layout post 1 -->
		                    </div>
		                    <div class="col col_4_of_12">
		                        <!-- Layout post 1 -->
		                        <div class="layout_post_1">
		                            <div class="item_thumb">
		                                <div class="thumb_icon">
		                                    <a href="post_single.html" jQuery><i class="fa fa-copy"></i></a>
		                                </div>
		                                <div class="thumb_hover">
		                                    <a href="post_single.html"><img src="demo/500x500/13.jpg" alt="Post"></a>
		                                </div>
		                                <div class="thumb_meta">
		                                    <span class="category" jQuery><a href="blog.html">Fashion</a></span>
		                                    <span class="comments"><a href="post_single.html">15 Comments</a></span>
		                                </div>
		                            </div>
		                            <div class="item_content">
		                                <h4><a href="post_single.html">Maecenas tempor volutpat commodo uspendisse potenti</a></h4>
		                                <div class="item_meta clearfix">
		                                    <span class="meta_date">October 3, 2014</span>
		                                    <span class="meta_likes"><a href="#">29</a></span>
		                                </div>
		                                <p>In et consequat nisi, at arcu. In hac habitasse platea dictumst. Fusce vestibulum tincidunt magna vitae scelerisque. Pellentesque venenatis velit ut feugia [...]</p>
		                            </div>
		                        </div><!-- End Layout post 1 -->
		                    </div>
		                    <div class="col col_4_of_12">
		                        <!-- Layout post 1 -->
		                        <div class="layout_post_1">
		                            <div class="item_thumb">
		                                <div class="thumb_icon">
		                                    <a href="post_single.html" jQuery><i class="fa fa-copy"></i></a>
		                                </div>
		                                <div class="thumb_hover">
		                                    <a href="post_single.html"><img src="demo/500x500/11.jpg" alt="Post"></a>
		                                </div>
		                                <div class="thumb_meta">
		                                    <span class="category" jQuery><a href="blog.html">Music</a></span>
		                                    <span class="comments"><a href="post_single.html">15 Comments</a></span>
		                                </div>
		                            </div>
		                            <div class="item_content">
		                                <h4><a href="post_single.html">Maecenas tempor volutpat commodo uspendisse potenti</a></h4>
		                                <div class="item_meta clearfix">
		                                    <span class="meta_date">October 3, 2014</span>
		                                    <span class="meta_likes"><a href="#">29</a></span>
		                                </div>
		                                <p>In et consequat nisi, at arcu. In hac habitasse platea dictumst. Fusce vestibulum tincidunt magna vitae scelerisque. Pellentesque venenatis velit ut feugia [...]</p>
		                            </div>
		                        </div><!-- End Layout post 1 -->
		                    </div>

		                </div>
	                    <ul class="page-numbers">
	                        <li><span class="page-numbers current">1</span></li>
	                        <li><a class="page-numbers" href="#">2</a></li>
	                        <li><a class="page-numbers" href="#">3</a></li>
	                        <li><a class="page-numbers" href="#">4</a></li>
	                    </ul><!-- End Pagination -->

                    </div>

                     
                </div>
            </section><!-- End Section -->
			
@endsection