@extends('website.template')
@section('title')
	{{$config->name}}
	@if(isset($selected_category))
		, CATEGORY {{$selected_category->name}}
	@endif

	@if(isset($selected_tag))
		, TAG {{$selected_tag->name}}
	@endif
@endsection

@section('content')
 <!-- Section -->
    <section>
        <div class="container">
            <div class="row">
                <div class="panel_title">
                    <div>
                        <h4>{{$selected_tag->name}}</h4>
                    </div>
                </div><!-- End Panel title -->

                <div class="row">

                    <div class="col col_8_of_12">
                        <!-- Content slider -->
                        <div class="content_slider">
                            <ul>
                                <li>
                                    <a href="post_single.html"><img src="{{base_url('assets')}}/demo/810x400.png">

                                    </a>
                                    <div class="slider_caption">
                                        <div class="thumb_meta">
                                            <span class="category" jQuery>
                                            <a href="blog.html">{{$selected_tag->name}}</a></span>
                                        </div>
                                        <div class="thumb_link">
                                            <h3><a href="post_single.html">Suspendisse porta quam eget nibh rhoncus eget ornare urna varius ed ut mauris augu uspendisse viverra elit libero</a></h3>
                                        </div>
                                    </div>
                                </li><!-- End Item -->
                             
                            </ul>
                        </div><!-- End Content slider -->   
                    </div>

                    <div class="col col_4_of_12">
                        <div class="widget">
                            <div class="clearfix">
                                <a href="#" target="_blank">
                                    <img src="{{base_url('assets')}}/demo/500x500/1.jpg" alt="Banner">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="rows">
                    @foreach($news as $result)
                    <div class="col col_4_of_12">
                        <!-- Layout post 1 -->
                        <div class="col col_12_of_12">
                        <div class="layout_post_1">
                            <div class="item_thumb">
                                <div class="thumb_hover">
                                    <a href="{{$result->url}}"><img src="{{$result->imagedir}}"style="object-fit: cover;height: 250px;" alt="{{$result->name}}"></a>
                                </div>
                                <div class="thumb_meta">
                                   <!--contoh -->
                                    @foreach($category->where('id', $result->id_category) as $cate)
                                        <span class="category" jQuery>
                                        <a href="{{$result->url}}">{{ $cate->name }}</a></span>
                                    @endforeach

                            </div>
                                </div>
                            </div>
                            <div class="item_content">
                                <h4><a href="{{$result->url }}">{{$result->name}}</a></h4>
                                <div class="item_meta clearfix">
                                    <span class="meta_date">{!! waktu_lalu($result->created_at) !!}</span>
                                    <span class="fa fa-eye">{!! k_view($result->view) !!}</span>
                                </div>
                                <p>{{ read_more($result->description,120) }}</p>
                            </div>
                        </div><!-- End Layout post 1 -->
                    </div>
                    @endforeach

                </div>
                </div>
                <ul class="page-numbers">
                    {!! $pagination !!}
                </ul><!-- End Pagination -->

            </div>

             
        </div>
    </section><!-- End Section -->
   
@endsection