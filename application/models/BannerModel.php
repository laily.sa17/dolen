<?php

class BannerModel extends MY_Model
{
	protected $table 	= "banner";
	protected $appends 	= array('imagedir','videodir','url','urlupdate','urldelete');

	public function getImagedirAttribute()
		{
		if($this->type=="image"){
			if (!$this->image|| !file_exists("images/banner/{$this->image}")) {
				return img_holder();
			}
			return base_url("images/banner/{$this->image}");	
		}
		else{
			return youtube_preview($this->video);
		}
	}
	public function getVideodirAttribute()
	{
		return youtube_iframe($this->video);
	}

	public function getUrlAttribute()
	{			
		return base_url("main/banner/detail/{$this->id}/".seo($this->name));
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/banner/update/'.$this->id);
	}

	public function getUrldeleteAttribute()
	{
		return base_url('superuser/banner/delete/'.$this->id);
	}
	public function scopeType($query,$type){
		return $query->where("type",$type);
	}

	public function scopeNotDraft($query){
		return $query->where("status",0);
	}


}