<?php

class CategoryBlogImagesModel extends MY_Model
{
	protected $table 	= "categoryblog_image";
	protected $appends 	= array('imagedir');


	public function getImagedirAttribute()
	{
		
		if (!$this->image|| !file_exists("images/categoryblog/{$this->image}")) {
			return img_holder();
		}

		return base_url("images/categoryblog/{$this->image}");	
	}

}
