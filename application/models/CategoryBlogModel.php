<?php

class CategoryBlogModel extends MY_Model{
	protected $table 	= "category";
	protected $appends 	= array('imagedir','url','urlupdate','urldelete');

    public function blog()
	{
		return $this->hasMany('BlogModel', 'id_category', 'id');
	}

	public function images(){
		return $this->hasMany('CategoryBlogImagesModel','id_category','id');
	}

	public function getImagedirAttribute()
	{
		if (!$this->image|| !file_exists("images/categoryblog/{$this->image}")) {
			return img_holder();
		}

		return base_url("images/categoryblog/{$this->image}");	
	}

	public function getUrlAttribute()
	{			
		return base_url("main/kategori/{$this->id}/".seo($this->name));
	}

	public function getUrlupdateAttribute()
	{
		return base_url('superuser/categoryblog/update/'.$this->id.'/'.seo($this->name));
	}

	public function getUrldeleteAttribute()
	{
		return base_url('superuser/categoryblog/delete/'.$this->id);
	}
}
