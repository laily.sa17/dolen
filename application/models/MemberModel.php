<?php

class MemberModel extends MY_Model
{
	protected $table 	= "member";
	protected $appends 	= array();

	public function blog()
	{
		return $this->hasMany('BlogModel', 'author', 'id');
	}

	public function scopeStatus($query,$status){
		return $query->where("status",$status);
	}

}
