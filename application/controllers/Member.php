<?php

class Member extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->data['config'] = ConfigModel::find(1);
        $this->data['seo'] = SeoModel::find(1);
        $this->data['sosmed'] = SosmedModel::desc()->get();
        $this->data['news'] = BlogModel::notDraft()->desc()->take(3)->get();
        $this->data['popular'] = BlogModel::notDraft()->orderBy('view', 'desc')->take(3)->get();
        $this->data['news'] = BlogModel::notDraft()->desc()->take(3)->get();
        $this->data['menupengunjung'] = MenupengunjungModel::Aktif()->orderby('urutan', 'asc')->get();
        $this->data['menu'] = "member";
        $this->data['satu']         = BannerModel::find(1);
        $this->data['dua']          = BannerModel::find(4);
        $this->data['tiga']         = BannerModel::find(5);
        $this->data['empat']        = BannerModel::find(6);
        $this->data['lima']         = BannerModel::find(7);
        $this->data['enam']         = BannerModel::find(8);
        $this->data['name'] = $this->session->userdata('member_name');
        $this->data['terbaru'] = BlogModel::notDraft()->desc()->take(4)->get();
        //statistik pengunjung
        $ip = $_SERVER['REMOTE_ADDR']; // Mendapatkan IP komputer user
        $tanggal = date("Ymd"); // Mendapatkan tanggal sekarang
        $waktu = time();
        $date_min = strtotime("-1 day");
        $kemarin = date('Y-m-d', $date_min);
        $tahun_ini = date('Y');
        $bulan_ini = date('m');
        $bataswaktu = time() - 300;

        // Mencek berdasarkan IPnya, apakah user sudah pernah mengakses hari ini
        $s = StatistikModel::where('ip', $ip)->where('tanggal', $tanggal)->get();

        if (count($s) == 0) {
            $statistik = new StatistikModel;
            $statistik->ip = $ip;
            $statistik->tanggal = $tanggal;
            $statistik->hits = 1;
            $statistik->online = $waktu;
            $statistik->save();
        } else {
            $statis = StatistikModel::where('ip', $ip)->get()->first();
            $statis->ip = $ip;
            $statis->tanggal = $tanggal;
            $statis->hits = $statis->hits + 1;
            $statis->online = $waktu;
            $statis->save();
        }

        $this->data['pengunjung'] = StatistikModel::where('tanggal', $tanggal)->groupBy('ip')->count('hits');
        $this->data['kemarin'] = StatistikModel::where('tanggal', $kemarin)->get()->count('hits');
        $this->data['perbulan'] = StatistikModel::where('tanggal', 'LIKE', '%' . $tahun_ini . '-' . $bulan_ini . '%')->get()->count('hits');
        $this->data['pertahun'] = StatistikModel::where('tanggal', 'LIKE', '%' . $tahun_ini . '%')->get()->count('hits');
        $this->data['total'] = StatistikModel::get()->count('hits');
        $this->data['online'] = StatistikModel::where('online', '>', $bataswaktu)->get()->count('hits');
        $this->data['category'] = CategoryBlogModel::desc()->get();

        $this->blade->sebarno('ctrl', $this);
    }

    public function index($page = "index", $id = null)
    {
        $data = $this->data;
        if (!$this->session->userdata('member_auth')) {
            redirect('member/masuk');
        }
        $data['menu'] = "member";
        $data['profile'] = MemberModel::find($this->session->userdata('member_id'));
        $data['rank'] = MemberModel::get();
        $data['submenu'] = 'dashboard';
        echo $this->blade->nggambar('website.member.index', $data);
    }

    public function masuk($url = 'index')
    {
        $data = $this->data;
        $data['menu'] = 'member';
        if ($this->session->userdata('member_auth')) {
            redirect('member');
        }
        if ($url == 'auth' && $this->input->is_ajax_request() == true) {
            if ($this->session->userdata('member_auth')) {
                echo goResult(false, "Anda Telah Masuk Member.");
                return;
            }

            if (strpos($this->input->post('username'), '@') > 1) {
                $where = array(
                    'email' => $this->input->post('username'),
                );
            } else {
                $where = array(
                    'username' => $this->input->post('username'),
                );
            }
            $auth = MemberModel::where($where)->first();

            if (!isset($auth->id)) {
                echo goResult(false, "Opps! Username Tidak Di Temukan");
                return;
            }

            if (DefuseLib::decrypt($auth->password) !== $this->input->post('password')) {
                echo goResult(false, "Opps! Maaf Password Anda Tidak Cocok");
                return;
            }

            if ($auth->status == "blocked") {
                echo goResult(false, "Opps! Maaf Akun Anda Di Blokir");
                return;
            }

            $auth->lastlog = date('Y-m-d H:i:s ');
            $auth->ipaddress = $this->input->ip_address();
            $auth->save();

            $newdata = array(
                'member_auth' => TRUE,
                'member_id' => $auth->id,
                'member_name' => $auth->name
            );

            $this->session->set_userdata($newdata);

            echo goResult(true, "Selamat datang...");
            return;
        } else {
            echo $this->blade->nggambar('website.member.login', $data);
        }
    }

    public function daftar($url = 'index')
    {
        $data = $this->data;
        $data['menu'] = "daftar";

        if ($url == 'confirm' && $this->input->is_ajax_request() == true) {
            $rules = [
                'required' => [
                    ['name'],
                    ['email'],
                    ['username'],
                    ['password']
                ],
                'lengthMin' => [
                    ['username', 5],
                    ['password', 8]
                ]
            ];

            $validate = Validation::check($rules, 'post');
            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            $cekmail = MemberModel::where('email', $this->input->post('email'))->first();
            $cekuser = MemberModel::where('username', $this->input->post('username'))->first();
            if (isset($cekmail->id)) {
                echo goResult(false, "Opps! Email telah digunakan.");
                return;
            } else if (isset($cekuser->id)) {
                echo goResult(false, "Opps! Username telah digunakan.");
                return;
            } else if ($this->input->post('password') !== $this->input->post('repassword')) {
                echo goResult(false, "Opps! Confirmation password not match.");
                return;
            }

            $daftar = new MemberModel;

            $daftar->name = $this->input->post('name');
            $daftar->username = $this->input->post('username');
            $daftar->email = $this->input->post('email');
            $daftar->password = DefuseLib::encrypt($this->input->post('password'));
            $daftar->status = 'pending';
            $daftar->ipaddress = $this->input->ip_address();

            $daftar->save();
            echo goResult(true, "Ok! Pendaftaran berhasil.");
        } else {
            echo $this->blade->nggambar('website.member.register', $data);
        }
    }

    public function lostpassword($url = "index")
    {
        $data = $this->data;
        if ($this->session->userdata('member_auth')) {
            redirect('member');
        }
        if ($url == "submit") {
            $where = array(
                'email' => $this->input->post('email'),
            );
            $member = MemberModel::where($where)->first();
            if (!isset($member->id)) {
                echo goResult(false, "Email tidak ditemukan.");
            } else {
                echo goResult(true, "Silakan cek email untuk mengembalikan password.");
            }
        } else {
            echo $this->blade->nggambar('website.member.login', $data);
            return;
        }
    }

    public function profile($url = 'index')
    {
        $data = $this->data;
        $data['menu'] = "profile";
        $data['profile'] = MemberModel::find($this->session->userdata('member_id'));

        $member = MemberModel::find($this->session->userdata('member_id'));

        if ($url == 'confirm') {
            $rules = [
                'required' => [
                    ['name'],
                    ['image']
                ]
            ];

            $validate = Validation::check($rules, 'post');
            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == false) {
                echo goResult(false, "Gambar Tidak ada");
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == true) {

                $filename = 'PROFILE__' . seo($this->input->post('name')) . '__' . date('Ymdhis');

                $upload = $this->upload('images/profile/', 'image', $filename);

                if ($upload['auth'] == false) {
                    echo goResult(false, $upload['msg']);
                    return;
                }

                if (!empty($upload['msg']['file_name'])) {
                    remFile(__DIR__ . '/../../public/images/profile/' . $member->image);
                }

                $member->image = $upload['msg']['file_name'];
            }

            $member->name = $this->input->post('name');
//            $member->image = $this->input->post('bla');

            $member->save();
            echo goResult(true, "Ok! Profile telah terupdate.");
        } else {
            echo $this->blade->nggambar('website.member.profile', $data);
        }
    }

    public function updatePassword($url = 'index')
    {
        $data = $this->data;
        $data['profile'] = MemberModel::find($this->session->userdata('member_id'));
        $data['menu'] = "ubah";

        $member = MemberModel::find($this->session->userdata('member_id'));

        if ($this->session->userdata('member_auth')) {
            redirect('member');
        }
        if ($url == 'confirm' && $this->input->is_ajax_request() == true) {
            if (DefuseLib::decrypt($member->password) !== $this->input->post('pass_lama')) {
                echo goResult(false, "Opps! Maaf Password Anda Tidak Cocok");
                return;
            } else {
                $rules = [
                    'required' => [
                        ['password']
                    ],
                    'lengthMin' => [
                        ['password', 8]
                    ]
                ];
                $validate = Validation::check($rules, 'post');
                if (!$validate->auth) {
                    echo goResult(false, $validate->msg);
                    return;
                }
                if ($this->input->post('password') !== $this->input->post('repassword')) {
                    echo goResult(false, "Opps! Confirmation password not match.");
                    return;
                }

//                $ubah    			= $member;

                $member->password = DefuseLib::encrypt($this->input->post('password'));

                $member->save();

                echo goResult(true, "Ok! Password telah terupdate.");
            }


        } else {
            echo $this->blade->nggambar('website.member.updatePassword', $data);
        }

    }

    // --------------------------------- START 	BLOG
    public function post($url = 'index', $id = null)
    {
        $data = $this->data;

        $data['menu'] = "blog";
        $data['category'] = CategoryBlogModel::desc()->get();
        $data['tag'] = TagModel::desc()->get();
        $data['profile'] = MemberModel::find($this->session->userdata('member_id'));
        $data['blog'] = BlogModel::with('category')->where('author', $this->session->userdata('member_id'))->desc()->get();


        if ($url == "create") {
            $data['type'] = "create";
            echo $this->blade->nggambar('website.member.post.content', $data);
            return;
        } else if ($url == "created" && $this->input->is_ajax_request() == true) {

            $rules = [
                'required' => [
                    ['name'], ['description'], ['category']
                ]
            ];

            $validate = Validation::check($rules, 'post');

            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            $blog = new BlogModel;

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == false) {
                echo goResult(false, "Opss! Gambar Tidak Ada Atau Tidak Sesuai");
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == true) {

                $filename = 'BLOG__' . seo($this->input->post('name')) . '__' . date('Ymdhis');

                $upload = $this->upload('images/blog', 'image', $filename);
                if ($upload['auth'] == false) {
                    echo goResult(false, $upload['msg']);
                    return;
                }

                $blog->image = $upload['msg']['file_name'];
            }


            $blog->name = $this->input->post('name');
            $blog->description = $this->input->post('description');
            $blog->author = $this->session->userdata('member_id');
            $blog->status = (null !== $this->input->post('status') ? 0 : 1);
            $blog->pilihan = (null !== $this->input->post('pilihan') ? 1 : 0);

            $category = CategoryBlogModel::find($this->input->post('category'));

            if (!isset($category->id)) {
                echo goResult(false, "Opps! Kategori Tidak Di Temukan");
            }
            $blog->id_category = $category->id;

            if ($blog->save()) {
                $tag = $this->input->post('tag');
                if ($tag) {
                    $tag = TagModel::whereIn('id', $tag)->desc()->get();
                    foreach ($tag as $result) {
                        $insertTag = new TagBlogModel;
                        $insertTag->id_blog = $blog->id;
                        $insertTag->id_tag = $result->id;
                        $insertTag->save();
                    }
                }

                echo goResult(true, "Sukses! , Blog Baru Telah Di Tambahkan");
                return;
            }
        } else if ($url == "update" && $id != null) {

            $data['blog'] = BlogModel::with('category', 'tags')->find($id);

            $data['blogtag'] = [];

            foreach ($data['blog']->tags as $result) {
                array_push($data['blogtag'], $result->id_tag);
            }

            $data['type'] = "update";

            if (!isset($data['blog']->id)) {
                redirect('member/post');
                return;
            }
            if ($data['blog']->author != $this->session->userdata('member_id')) {
                redirect('member/post');
                return;
            }

            echo $this->blade->nggambar('website.member.post.content', $data);
            return;
        } else if ($url == "updated" && $id != null && $this->input->is_ajax_request() == true) {

            $blog = BlogModel::find($id);

            if (!isset($blog->id)) {
                echo goResult(false, "Opss! Blog Tidak Di Temukan");
                return;
            }

            $rules = [
                'required' => [
                    ['name'], ['description'], ['category']
                ]
            ];

            $validate = Validation::check($rules, 'post');

            if (!$validate->auth) {
                echo goResult(false, $validate->msg);
                return;
            }

            if (!empty($_FILES['image']['name']) && $this->isImage('image') == true) {

                $filename = 'BLOG__' . seo($this->input->post('name')) . '__' . date('Ymdhis');

                $upload = $this->upload('images/blog/', 'image', $filename);

                if ($upload['auth'] == false) {
                    echo goResult(false, $upload['msg']);
                    return;
                }

                if (!empty($upload['msg']['file_name'])) {
                    remFile(__DIR__ . '/../../public/images/blog/' . $blog->image);
                }

                $blog->image = $upload['msg']['file_name'];
            }

            $blog->name = $this->input->post('name');
            $blog->description = $this->input->post('description');
            $blog->status = (null !== $this->input->post('status') ? 0 : 1);
            $blog->pilihan = (null !== $this->input->post('pilihan') ? 1 : 0);

            $category = CategoryBlogModel::find($this->input->post('category'));
            if (!isset($category->id)) {
                echo goResult(false, "Opps! Kategori Tidak Di Temukan");
            }

            $blog->id_category = $category->id;

            if ($blog->save()) {

                $tag = $this->input->post('tag');

                if ($tag) {

                    TagBlogModel::where('id_blog', $blog->id)->delete();

                    $tag = TagModel::whereIn('id', $tag)->desc()->get();
                    foreach ($tag as $result) {
                        $insertTag = new TagBlogModel;
                        $insertTag->id_blog = $blog->id;
                        $insertTag->id_tag = $result->id;
                        $insertTag->save();
                    }
                }

                echo goResult(true, "Blog Telah Di Perbarui");
                return;
            }
        } else if ($url == "delete" && $id != null) {

            $blog = BlogModel::find($id);

            if (!isset($blog->id)) {
                redirect('member/post');
                return;
            }

            TagBlogModel::where('id_blog', $blog->id)->delete();

            if ($blog->image != "") {
                if (file_exists("images/blog/{$blog->image}")) {
                    remFile(__DIR__ . '/../../public/images/blog/' . $blog->image);
                }
            }

            $blog->delete();

            redirect('member/post');
        }
          else if ($url == "createTag") {
            $data['type'] = "create";
            echo $this->blade->nggambar('website.member.post.content', $data);
            return;

        } else if ($url == "tagCreated" && $this->input->is_ajax_request() == true) {

              $rules = [
                  'required' => [
                      ['name'], ['description']
                  ]
              ];

              $validate = Validation::check($rules, 'post');

              if (!$validate->auth) {
                  echo goResult(false, $validate->msg);
                  return;
              }

              $tag = new TagModel;

              $tag->name = $this->input->post('name');
              $tag->description = $this->input->post('description');

              if ($tag->save()) {
                  echo goResult(true, "Tag Blog Telah Di Tambahkan");
                  return;
              }
          }else {
                  echo $this->blade->nggambar('website.member.post.index', $data);
                  return;
              }
          }

// --------------------------------- END BLOG


    public function logout()
    {
        if (!$this->session->userdata('member_auth')) {
            redirect('auth');
            return;
        }
        $this->session->unset_userdata('member_auth');
        $this->session->unset_userdata('member_id');
        $this->session->unset_userdata('member_name');
        redirect('member');
    }

    private function validation($rules, $type)
    {
        if ($type == "post") {
            $v = new Valitron\Validator($_POST);
        } else {
            $v = new Valitron\Validator($_GET);
        }


        $v->rules($rules);
        if ($v->validate()) {
            return true;
        } else {
            return false;
        }
    }

    private function upload($dir, $name = 'userfile', $filename = false)
    {
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 3000;

        if ($filename) {
            $config['file_name'] = $filename;
        } else {
            $config['encrypt_name'] = TRUE;
        }

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
            $data['auth'] = false;
            $data['msg'] = $this->upload->display_errors();
            return $data;
        } else {
            $data['auth'] = true;
            $data['msg'] = $this->upload->data();
            return $data;
        }
    }

    private function isImage($file)
    {
        if ((($_FILES[$file]['type'] == 'image/gif') || ($_FILES[$file]['type'] == 'image/jpeg') || ($_FILES[$file]['type'] == 'image/png'))) {
            return true;
        } else {
            return false;
        }
    }

    private function upload_materi($dir, $name = 'userfile', $filename = false)
    {
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'doc|docx|jpg|png|jpeg|pdf';
        $config['max_size'] = 5000;

        if ($filename) {
            $config['file_name'] = $filename;
        } else {
            $config['encrypt_name'] = TRUE;
        }

        $this->load->library('upload', $config);

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
            $data['auth'] = false;
            $data['msg'] = $this->upload->display_errors();
            return $data;
        } else {
            $data['auth'] = true;
            $data['msg'] = $this->upload->data();
            return $data;
        }
    }

    private function isDocument($file)
    {
        if ($_FILES[$file]['type'] == 'application/msword' || $_FILES[$file]['type'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || $_FILES[$file]['type'] == 'application/pdf' || $_FILES[$file]['type'] == 'application/pdf') {

            return true;
        } else {
            return false;
        }
    }
}