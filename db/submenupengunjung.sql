-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2018 at 10:24 AM
-- Server version: 10.0.36-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.38-1+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dolen`
--

-- --------------------------------------------------------

--
-- Table structure for table `submenupengunjung`
--

CREATE TABLE `submenupengunjung` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `urutan` int(5) NOT NULL,
  `aktif` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submenupengunjung`
--

INSERT INTO `submenupengunjung` (`id`, `id_menu`, `judul`, `link`, `urutan`, `aktif`, `created_at`, `updated_at`) VALUES
(1, 2, 'Visi dan Misi', 'main/halaman/1/visi-dan-misi', 1, '0', '2017-08-16 03:20:38', '2017-08-16 03:20:38'),
(2, 2, 'Sejarah', 'main/halaman/2/sejarah', 2, '0', '2017-08-16 03:20:59', '2017-08-16 03:20:59'),
(3, 2, 'Manajemen', 'main/halaman/3/manajemen', 3, '0', '2017-08-16 06:38:03', '2017-08-16 06:38:03'),
(4, 2, 'Mitra', 'main/halaman/4/mitra', 4, '0', '2017-08-16 06:39:31', '2017-08-16 06:39:31'),
(5, 2, 'Project', 'main/project', 5, '0', '2017-08-16 06:36:15', '2017-08-16 06:36:15'),
(6, 2, 'Layanan', 'main/service', 6, '0', '2017-08-16 06:42:19', '2017-08-16 06:42:19'),
(7, 2, 'Partner', 'main/partner', 7, '0', '2017-08-16 06:38:34', '2017-08-16 06:38:34'),
(8, 4, 'Offline', 'https://www.google.co.id/maps/place/Topsell+Mojokerto/@-7.4910324,112.4232811,17z/data=!3m1!4b1!4m5!3m4!1s0x2e781283d0bd31b7:0x5d7e63fc5836f710!8m2!3d-7.4910324!4d112.4254698', 1, '0', '2017-08-21 07:02:47', '2017-08-21 07:02:47'),
(9, 4, 'Online', 'main/product', 2, '0', '2017-08-21 07:03:18', '2017-08-21 07:03:18'),
(10, 6, 'Photo', 'main/gallery/photo', 1, '1', '2018-10-31 03:23:10', '0000-00-00 00:00:00'),
(11, 6, 'Video', 'main/gallery/video', 2, '1', '2018-10-31 03:23:10', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `submenupengunjung`
--
ALTER TABLE `submenupengunjung`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `submenupengunjung`
--
ALTER TABLE `submenupengunjung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
