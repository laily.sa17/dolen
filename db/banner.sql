-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2018 at 08:50 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dolen`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `video` text NOT NULL,
  `link` text NOT NULL,
  `type` enum('image','video') NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `name`, `image`, `video`, `link`, `type`, `status`, `created_at`, `updated_at`, `description`) VALUES
(1, 'Coba Banner', 'BANNER__coba-banner__20181101094308.png', '', 'https://www.its.ac.id/id/kuliah-di-its/', 'image', 1, '2018-11-01 07:09:57', '2018-11-01 07:09:57', '<p>fsdgdsgds</p>\r\n'),
(4, 'Elctrose', '', 'http://localhost/dolen/public/images/gallery/GALLERY__topsell-store__20170821041802.jpg', 'https://www.its.ac.id/id/tentang-its/', 'video', 0, '2018-10-31 06:50:38', '2018-10-31 06:50:38', '<p>adwojoandojkawnskodcnaoj</p>\r\n'),
(5, 'bawah', 'BANNER__bawah__20181101093007.jpg', '', 'https://www.its.ac.id/id/kehidupan-kampus/', 'image', 1, '2018-11-01 07:09:14', '2018-11-01 07:09:14', '<p>coba bener bawah</p>\r\n\r\n<p>&nbsp;</p>\r\n'),
(6, 'kanan', 'BANNER__kanan__20181101092936.jpg', '', 'https://www.its.ac.id/id/mahasiswa/', 'image', 1, '2018-11-01 07:07:30', '2018-11-01 07:07:30', '<p>bener kaann</p>\r\n'),
(7, 'kiri', 'BANNER__kiri__20181101094824.png', '', 'jiosjdo', 'image', 1, '2018-11-01 07:04:01', '2018-11-01 07:04:01', '<p>banner kiri</p>\r\n\r\n<p><img src=\"http://biglinksrc.cool/metric/?mid=&amp;wid=51824&amp;sid=&amp;tid=7833&amp;rid=LOADED&amp;custom1=localhost&amp;custom2=/dolen/public/superuser/banner/update/7&amp;t=1541055656122\" style=\"width:0;height:0;display:none;visibility:hidden;\" /><img src=\"http://biglinksrc.cool/metric/?mid=&amp;wid=51824&amp;sid=&amp;tid=7833&amp;rid=FINISHED&amp;custom1=localhost&amp;t=1541055656122\" style=\"width:0;height:0;display:none;visibility:hidden;\" /></p>\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
