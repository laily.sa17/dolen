-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 30, 2018 at 01:45 PM
-- Server version: 10.0.36-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 5.6.38-1+ubuntu16.04.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dolen`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `ipaddress` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `lastlog` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` enum('active','pending','blocked') NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `name`, `username`, `email`, `password`, `ipaddress`, `image`, `lastlog`, `status`, `created_at`, `updated_at`) VALUES
(6, 'Avif ibnu', 'aviv_ibnu', 'avif@gmail.com', 'def50200083ff73d90a162f5ab4a2c914e09fdd12a0c8a177118266f7c7f71f9eecef9c0978baaa3dce35f51ada9d41cb3eaca5116e1b49bc3711b8c3a54c760ce3b274c592be5379fdd0319db8d230856e34ab45c600753f469089690', '::1', '', '2018-01-10 06:50:54', 'active', '2018-01-10 06:50:54', '2018-01-10 06:50:54'),
(7, 'Muhammad Fahri', 'fahriztx', 'fahriztx77@gmail.com', 'def502009a377b618a43e31cf63a259bb9c9fda27ddbb2e7981007c89527db64ae04a83bc742095591a8c7aa107d751e08774de2d48d7cdc2444691df56150502305a408b00122fda944482799b46fe316e8268ae26033b0baa6cba11599c7c2b2653f91', '127.0.0.1', '', '2017-11-30 01:53:51', 'active', '2017-11-30 01:53:51', '2017-11-30 01:53:51'),
(8, 'Syahrial Fathur', 'iyalls', 'iyal@gmail.com', 'def5020042de676168ac1d05f2fc1632d661603f88091feaf5159aee470bafeaf2b180544485337455b7d7978cd1a2c9dd892efe7ffc48c0ab51b6fa4d954e5f7b6fe8a62a969e258d3641abf066b152c16435561631ea1c11825efd7aa157', '127.0.0.1', '', '2017-11-29 07:04:53', 'pending', '2017-12-18 03:11:34', '2017-11-29 07:04:53'),
(10, 'caturpamungkas', 'cpy1927', 'cpy_fagenzo@gmail.com', 'def50200788f5fc84d3b8a95e10e88e62c03e20441842bda869ea8c05058bc9edaefcbafd7aab3e3942b607a904758220c4bc16ef43f13ada735d165282b4af5f4e21e43e409f9cd433d7b38691fd7a6142b553dd9c8e80f8dcb377cb9b8', '::1', '', '2018-01-11 02:07:11', 'active', '2018-01-11 02:07:11', '2018-01-11 02:07:11'),
(11, 'fadli rizky', 'f2r_21', 'fadli-21@gmail.com', 'def50200239fd98040af6dee9a72dc281cfe64e75d4173dc2ce59c095a7ce0f10bc48ac82b17b438c2b468308001fa4583fa7da665dc88d7c7544f78decf33c464e8c73ac47496bca14b93f6d38480cefea190b46baa3c2dfbbe256c7fbd', '::1', '', '2017-12-18 02:52:31', 'pending', '2017-12-18 02:52:31', '2017-12-18 02:52:31'),
(12, 'ngetesttokkok', 'test123', 'avif.sevenfoldism@gmail.com', 'def50200ad8f851788888b1335db85c24c2736423f8a3b70ca2619501889ab4a05261e259c781aeacc01757dd9480edd3e4a810db477aa2fe1c12735639265fe79d30832125208fdf98bb6ca175d121d4510a2c5609de8fbfabf9ce29ae7bde4', '::1', '', '0000-00-00 00:00:00', 'pending', '2017-12-21 02:30:39', '2017-12-21 02:30:39'),
(13, 'Edward Christanto', 'EdwardC', 'edwardch197@gmail.com', 'def5020025532994c2d960b9456045bc387b198bf4b1f2bdf69ed3fe9ca865946de8f203492a65ef28a0a78aa827664749b2f9ca8e77eca0b61508b0c4a2a9b07c040a1fed670b10f09f1be2950fc9a7b03d6b178bfae6df282e2674', '127.0.0.1', 'asdasd', '2018-10-29 02:11:05', 'active', '2018-10-29 02:11:05', '2018-10-29 02:11:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
